package net.thumbtack.alertreporting.service;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.dao.UserDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.user.Role;
import net.thumbtack.alertreporting.entity.user.User;
import net.thumbtack.alertreporting.security.EncryptionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {
    @Mock
    private UserDAO userDAO;
    @Mock
    private CampaignDAO campaignDAO;
    @Mock
    private EncryptionService encryptionService;
    @InjectMocks
    private UserService userService = new UserService(userDAO, campaignDAO);

    @Test
    public void addUser() throws Exception {
        Role role = new Role("USER");
        User expectedUser = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));

        when(userDAO.getByUsername(expectedUser.getUsername())).thenReturn(null);
        when(encryptionService.encryptPassword(expectedUser.getPassword())).thenReturn(expectedUser.getPassword());
        when(userDAO.insert(expectedUser)).thenReturn(expectedUser);

        User actualUser = userService.addUser(expectedUser);

        assertNotNull(actualUser);
        assertEquals(expectedUser, actualUser);
        verify(userDAO, times(1)).getByUsername(expectedUser.getUsername());
        verify(encryptionService, times(1)).encryptPassword(expectedUser.getPassword());
        verify(userDAO, times(1)).insert(expectedUser);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addUserAlreadyExists() throws Exception {
        Role role = new Role("USER");
        User user = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));

        when(userDAO.getByUsername(user.getUsername())).thenReturn(user);

        userService.addUser(user);
    }

    @Test
    public void update() throws Exception {
        Role role = new Role("USER");
        User expectedUser = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));

        when(userDAO.getById(expectedUser.getId())).thenReturn(expectedUser);
        when(encryptionService.encryptPassword(expectedUser.getPassword())).thenReturn(expectedUser.getPassword());
        when(userDAO.update(expectedUser)).thenReturn(1);

        userService.update(expectedUser);

        verify(userDAO, times(1)).getById(expectedUser.getId());
        verify(encryptionService, times(1)).encryptPassword(expectedUser.getPassword());
        verify(userDAO, times(1)).update(expectedUser);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNotExist() throws Exception {
        Role role = new Role("USER");
        User user = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));

        when(userDAO.getById(user.getId())).thenReturn(null);

        userService.update(user);
    }

    @Test
    public void delete() throws Exception {
        when(userDAO.delete(0)).thenReturn(1);

        userService.delete(0);

        verify(userDAO, times(1)).delete(0);
    }

    @Test
    public void getAll() throws Exception {
        Role role = new Role("USER");
        User user = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));
        List<User> expectedUsers = Collections.singletonList(user);

        when(userDAO.getAll()).thenReturn(expectedUsers);

        List<User> actualUsers = userService.getAll();

        assertNotNull(actualUsers);
        assertEquals(expectedUsers, actualUsers);
        verify(userDAO, times(1)).getAll();
    }

    @Test
    public void get() throws Exception {
        Role role = new Role("USER");
        User expectedUser = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));
        expectedUser.setId(0);

        when(userDAO.getById(0)).thenReturn(expectedUser);

        User actualUser = userService.get(0);

        assertNotNull(actualUser);
        assertEquals(expectedUser, actualUser);
        verify(userDAO, times(1)).getById(0);
    }

    @Test
    public void assign() throws Exception {
        User user = new User();
        user.setId(0);
        Campaign campaign = new Campaign();
        campaign.setId(0);

        when(userDAO.getById(user.getId())).thenReturn(user);
        when(campaignDAO.getById(campaign.getId())).thenReturn(campaign);
        when(userDAO.getByCampaign(campaign)).thenReturn(null);
        when(userDAO.addCampaign(user.getId(), campaign.getId())).thenReturn(1);

        userService.assign(user.getId(), campaign.getId());

        verify(userDAO, times(1)).getById(user.getId());
        verify(campaignDAO, times(1)).getById(campaign.getId());
        verify(userDAO, times(1)).getByCampaign(campaign);
        verify(userDAO, times(1)).addCampaign(user.getId(), campaign.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void assignUserNotExist() throws Exception {
        Campaign campaign = new Campaign();
        campaign.setId(0);

        when(userDAO.getById(10)).thenReturn(null);
        when(campaignDAO.getById(campaign.getId())).thenReturn(campaign);
        when(userDAO.getByCampaign(campaign)).thenReturn(null);
        when(userDAO.addCampaign(10, campaign.getId())).thenReturn(null);

        userService.assign(10, campaign.getId());

        verify(userDAO, times(1)).getById(10);
        verify(campaignDAO, times(0)).getById(campaign.getId());
        verify(userDAO, times(0)).getByCampaign(campaign);
        verify(userDAO, times(0)).addCampaign(10, campaign.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void assignCampaignNotExist() throws Exception {
        User user = new User();
        user.setId(0);

        when(userDAO.getById(0)).thenReturn(user);
        when(campaignDAO.getById(10)).thenReturn(null);
        when(userDAO.getByCampaign(null)).thenReturn(null);
        when(userDAO.addCampaign(user.getId(), 10)).thenReturn(null);

        userService.assign(user.getId(), 10);

        verify(userDAO, times(1)).getById(0);
        verify(campaignDAO, times(1)).getById(10);
        verify(userDAO, times(0)).getByCampaign(null);
        verify(userDAO, times(0)).addCampaign(user.getId(), 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void assignManagerAlreadyAssigned() throws Exception {
        User user = new User();
        user.setId(0);
        Campaign campaign = new Campaign();
        campaign.setId(0);
        List<User> users = Collections.singletonList(new User());

        when(userDAO.getById(user.getId())).thenReturn(user);
        when(campaignDAO.getById(campaign.getId())).thenReturn(campaign);
        when(userDAO.getByCampaign(campaign)).thenReturn(users);
        when(userDAO.addCampaign(user.getId(), campaign.getId())).thenReturn(null);

        userService.assign(user.getId(), campaign.getId());

        verify(userDAO, times(1)).getById(user.getId());
        verify(campaignDAO, times(1)).getById(campaign.getId());
        verify(userDAO, times(1)).getByCampaign(campaign);
        verify(userDAO, times(0)).addCampaign(user.getId(), campaign.getId());
    }

    @Test
    public void unassign() throws Exception {
        User user = new User();
        user.setId(0);
        Campaign campaign = new Campaign();
        campaign.setId(0);
        List<User> users = Collections.singletonList(user);

        when(userDAO.getById(user.getId())).thenReturn(user);
        when(campaignDAO.getById(campaign.getId())).thenReturn(campaign);
        when(userDAO.getByCampaign(campaign)).thenReturn(users);
        when(userDAO.removeCampaign(user.getId(), campaign.getId())).thenReturn(1);

        userService.unassign(user.getId(), campaign.getId());

        verify(userDAO, times(1)).getById(user.getId());
        verify(campaignDAO, times(1)).getById(campaign.getId());
        verify(userDAO, times(1)).getByCampaign(campaign);
        verify(userDAO, times(1)).removeCampaign(user.getId(), campaign.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void unassignUserNotExist() throws Exception {
        Campaign campaign = new Campaign();
        campaign.setId(0);

        when(userDAO.getById(10)).thenReturn(null);
        when(campaignDAO.getById(campaign.getId())).thenReturn(campaign);
        when(userDAO.getByCampaign(campaign)).thenReturn(null);
        when(userDAO.removeCampaign(10, campaign.getId())).thenReturn(null);

        userService.unassign(10, campaign.getId());

        verify(userDAO, times(1)).getById(10);
        verify(campaignDAO, times(0)).getById(campaign.getId());
        verify(userDAO, times(0)).getByCampaign(campaign);
        verify(userDAO, times(0)).removeCampaign(10, campaign.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void unassignCampaignNotExist() throws Exception {
        User user = new User();
        user.setId(0);

        when(userDAO.getById(0)).thenReturn(user);
        when(campaignDAO.getById(10)).thenReturn(null);
        when(userDAO.getByCampaign(null)).thenReturn(null);
        when(userDAO.removeCampaign(user.getId(), 10)).thenReturn(null);

        userService.unassign(user.getId(), 10);

        verify(userDAO, times(1)).getById(0);
        verify(campaignDAO, times(1)).getById(10);
        verify(userDAO, times(0)).getByCampaign(null);
        verify(userDAO, times(0)).removeCampaign(user.getId(), 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void unassignUserWasntAssigned() throws Exception {
        User user = new User();
        user.setId(0);
        Campaign campaign = new Campaign();
        campaign.setId(0);
        List<User> users = Collections.singletonList(new User());

        when(userDAO.getById(user.getId())).thenReturn(user);
        when(campaignDAO.getById(campaign.getId())).thenReturn(campaign);
        when(userDAO.getByCampaign(campaign)).thenReturn(users);
        when(userDAO.removeCampaign(user.getId(), campaign.getId())).thenReturn(null);

        userService.unassign(user.getId(), campaign.getId());

        verify(userDAO, times(1)).getById(user.getId());
        verify(campaignDAO, times(1)).getById(campaign.getId());
        verify(userDAO, times(1)).getByCampaign(campaign);
        verify(userDAO, times(0)).removeCampaign(user.getId(), campaign.getId());
    }

    @Test
    public void getAllRoles() throws Exception {
        List<Role> expectedRoles = Collections.singletonList(new Role());

        when(userDAO.getAllRoles()).thenReturn(expectedRoles);

        List<Role> actualRoles = userService.getAllRoles();

        assertNotNull(actualRoles);
        assertEquals(expectedRoles, actualRoles);
        verify(userDAO, times(1)).getAllRoles();
    }
}