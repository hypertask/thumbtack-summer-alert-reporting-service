package net.thumbtack.alertreporting.service;

import net.thumbtack.alertreporting.dao.ReportDAO;
import net.thumbtack.alertreporting.entity.report.Report;
import net.thumbtack.alertreporting.service.validation.RuleFactory;
import net.thumbtack.alertreporting.service.validation.rule.AbstractRule;
import net.thumbtack.alertreporting.service.validation.rule.RuleInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class ReportServiceTest {
    @Mock
    private ReportDAO reportDAO;
    @Mock
    private RuleFactory ruleFactory;
    @InjectMocks
    private ReportService reportService = new ReportService(reportDAO);

    @Test
    public void validate() throws Exception {
        List<String> rules = new ArrayList<>();
        rules.add("ADVERTISER_REQUIRED_FIELDS_FILLED");

        AbstractRule rule = mock(AbstractRule.class);

        List<Report> reports = new ArrayList<>();
        Report report = new Report();
        reports.add(report);

        when(ruleFactory.getRule("ADVERTISER_REQUIRED_FIELDS_FILLED")).thenReturn(rule);
        when(rule.validate()).thenReturn(reports);
        when(reportDAO.insert(report)).thenReturn(report);

        reportService.validate(rules);

        verify(ruleFactory, times(1)).getRule("ADVERTISER_REQUIRED_FIELDS_FILLED");
    }

    @Test
    public void getAll() throws Exception {
        List<Report> expectedReports = new ArrayList<>();
        expectedReports.add(new Report());

        when(reportDAO.getAll()).thenReturn(expectedReports);

        List<Report> actualReports = reportService.getAll();

        assertNotNull(actualReports);
        assertEquals(expectedReports, actualReports);
        verify(reportDAO, times(1)).getAll();
    }

    @Test
    public void getUserReports() throws Exception {
        List<Report> expectedReports = new ArrayList<>();
        expectedReports.add(new Report());

        when(reportDAO.getAll()).thenReturn(expectedReports);

        List<Report> actualReports = reportService.getUserReports();

        assertNotNull(actualReports);
        assertEquals(expectedReports, actualReports);
        verify(reportDAO, times(1)).getAll();
    }

    @Test
    public void getRules() throws Exception {
        RuleInfo[] expectedRules = RuleInfo.values();

        RuleInfo[] actualRules = reportService.getRules();

        assertNotNull(actualRules);
        assertArrayEquals(expectedRules, actualRules);
    }
}