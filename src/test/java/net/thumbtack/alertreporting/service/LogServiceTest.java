package net.thumbtack.alertreporting.service;

import net.thumbtack.alertreporting.dao.LogDAO;
import net.thumbtack.alertreporting.entity.logmessage.LogMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class LogServiceTest {
    @Mock
    private LogDAO logDAO;
    @InjectMocks
    private LogService logService;

    @Test
    public void getAll() throws Exception {
        List<LogMessage> expectedMessages = Arrays.asList(new LogMessage(null, null, null));

        when(logDAO.getAll()).thenReturn(expectedMessages);

        List<LogMessage> actualMessages = logService.getAll();

        assertNotNull(actualMessages);
        assertEquals(expectedMessages, actualMessages);
        verify(logDAO, times(1)).getAll();
    }

}