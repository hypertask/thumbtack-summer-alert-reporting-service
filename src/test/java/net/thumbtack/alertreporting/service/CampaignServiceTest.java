package net.thumbtack.alertreporting.service;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CampaignServiceTest {
    @Mock
    private CampaignDAO campaignDAO;
    @InjectMocks
    private CampaignService campaignService = new CampaignService(campaignDAO);

    @Test
    public void getUserCampaigns() throws Exception {
        List<Campaign> expectedCampaigns = Collections.singletonList(new Campaign());

        when(campaignDAO.getByUserId(0)).thenReturn(expectedCampaigns);

        List<Campaign> actualCampaigns = campaignService.getUserCampaigns(0);

        assertNotNull(actualCampaigns);
        assertEquals(expectedCampaigns, actualCampaigns);
        verify(campaignDAO, times(1)).getByUserId(0);
    }

    @Test
    public void getNotAssigned() throws Exception {
        List<Campaign> expectedCampaigns = Collections.singletonList(new Campaign());

        when(campaignDAO.getNotAssigned()).thenReturn(expectedCampaigns);

        List<Campaign> actualCampaigns = campaignService.getNotAssigned();

        assertNotNull(actualCampaigns);
        assertEquals(expectedCampaigns, actualCampaigns);
        verify(campaignDAO, times(1)).getNotAssigned();
    }

}