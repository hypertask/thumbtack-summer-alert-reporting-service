package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.campaign.CampaignStatus;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.creative.CreativeStatus;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CampaignHasActiveCreativesRuleTest {
    @Mock
    private CampaignDAO campaignDAO;
    @InjectMocks
    private AbstractRule rule = new CampaignHasActiveCreativesRule();

    @Test
    public void validateFineCampaign() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaign.setStatus(CampaignStatus.ACTIVE);

        Creative creative = new Creative();
        creative.setStatus(CreativeStatus.ACTIVE);
        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateActiveCampaignWithoutCreatives() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaign.setStatus(CampaignStatus.ACTIVE);
        campaigns.add(campaign);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Active campaign has no creatives"));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateActiveCampaignWithoutActiveCreatives() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaign.setStatus(CampaignStatus.ACTIVE);

        Creative creative = new Creative();
        creative.setStatus(CreativeStatus.INACTIVE);
        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Active campaign has no active creatives"));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateNotActiveCampaignWithoutActiveStatus() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaign.setStatus(CampaignStatus.PAUSED);
        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }
}