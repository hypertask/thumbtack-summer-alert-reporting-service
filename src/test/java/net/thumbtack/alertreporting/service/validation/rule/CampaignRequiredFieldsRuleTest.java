package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.campaign.CampaignStatus;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


@RunWith(SpringJUnit4ClassRunner.class)
public class CampaignRequiredFieldsRuleTest {
    @Mock
    private CampaignDAO campaignDAO;
    @InjectMocks
    private AbstractRule rule = new CampaignRequiredFieldsRule();

    @Test
    public void validateCampaignWithDraftStatus() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaign.setStatus(CampaignStatus.DRAFT);
        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateFineCampaign() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaign.setName("test");
        campaign.setStatus(CampaignStatus.ACTIVE);
        campaign.setStartDate(new Date());
        campaign.setEndDate(new Date());
        campaign.setImpressionCap(1);
        campaign.setBudget(new BigDecimal(1));
        campaign.setPrice(new BigDecimal(1));
        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCampaignWithNullFields() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaigns.add(campaign);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Campaign name is empty"));
        issues.add(new Issue("Campaign status is empty"));
        issues.add(new Issue("Campaign start date is empty"));
        issues.add(new Issue("Campaign end date is empty"));
        issues.add(new Issue("Campaign impression cap is empty"));
        issues.add(new Issue("Campaign budget is empty"));
        issues.add(new Issue("Campaign has no price"));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCampaignWithEmptyFields() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaign.setName("");
        campaigns.add(campaign);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Campaign name is empty"));
        issues.add(new Issue("Campaign status is empty"));
        issues.add(new Issue("Campaign start date is empty"));
        issues.add(new Issue("Campaign end date is empty"));
        issues.add(new Issue("Campaign impression cap is empty"));
        issues.add(new Issue("Campaign budget is empty"));
        issues.add(new Issue("Campaign has no price"));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, "", issues, new Date());
        expectedReports.add(report);

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }
}