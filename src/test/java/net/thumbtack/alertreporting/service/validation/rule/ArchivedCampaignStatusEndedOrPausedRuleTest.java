package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.campaign.CampaignStatus;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class ArchivedCampaignStatusEndedOrPausedRuleTest {
    @Mock
    private CampaignDAO campaignDAO;
    @InjectMocks
    private AbstractRule rule = new ArchivedCampaignStatusEndedOrPausedRule();

    @Test
    public void validateArchivedCampaignWithActiveStatus() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();
        campaign.setStatus(CampaignStatus.ACTIVE);
        campaign.setArchived(true);
        campaigns.add(campaign);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Archived campaign has active or draft status"));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(campaignDAO.getByArchivedStatus(true)).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertNotNull(actualReports);
        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getByArchivedStatus(true);
    }

    @Test
    public void validateArchivedCampaignWithDraftStatus() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();
        campaign.setStatus(CampaignStatus.DRAFT);
        campaign.setArchived(true);
        campaigns.add(campaign);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Archived campaign has active or draft status"));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(campaignDAO.getByArchivedStatus(true)).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertNotNull(actualReports);
        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getByArchivedStatus(true);
    }

    @Test
    public void validateFineArchivedCampaign() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();
        campaign.setStatus(CampaignStatus.ENDED);
        campaign.setArchived(true);
        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getByArchivedStatus(true)).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertNotNull(actualReports);
        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getByArchivedStatus(true);
    }
}