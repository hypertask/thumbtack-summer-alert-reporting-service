package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.AdvertiserDAO;
import net.thumbtack.alertreporting.entity.advertiser.Advertiser;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class AdvertiserRequiredFieldsRuleTest {
    @Mock
    private AdvertiserDAO advertiserDAO;
    @InjectMocks
    private AbstractRule rule = new AdvertiserRequiredFieldsRule();

    @Test
    public void validateAdvertiserWithNullFields() throws Exception {
        List<Advertiser> advertisers = new ArrayList<>();

        Advertiser advertiser = new Advertiser();
        advertisers.add(advertiser);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Advertiser#%d name is empty", advertiser.getId()));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(advertiserDAO.getAll()).thenReturn(advertisers);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(advertiserDAO, times(1)).getAll();
    }

    @Test
    public void validateAdvertiserWithEmptyFields() throws Exception {
        List<Advertiser> advertisers = new ArrayList<>();

        Advertiser advertiser = new Advertiser();
        advertiser.setName("");
        advertisers.add(advertiser);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Advertiser#%d name is empty", advertiser.getId()));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(advertiserDAO.getAll()).thenReturn(advertisers);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(advertiserDAO, times(1)).getAll();
    }

    @Test
    public void validateFineAdvertiser() throws Exception {
        List<Advertiser> advertisers = new ArrayList<>();

        Advertiser advertiser = new Advertiser();
        advertiser.setName("test");
        advertisers.add(advertiser);

        List<Report> expectedReports = new ArrayList<>();

        when(advertiserDAO.getAll()).thenReturn(advertisers);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(advertiserDAO, times(1)).getAll();
    }
}