package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.creative.CreativeStatus;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CampaignActiveCreativesHaveSamePriceRuleTest {
    @Mock
    private CampaignDAO campaignDAO;
    @InjectMocks
    private AbstractRule rule = new CampaignActiveCreativesHaveSamePriceRule();

    @Test
    public void validateCampaignWithoutCreatives() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateFineCampaign() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();

        List<Creative> creatives = new ArrayList<>();
        Creative creative = new Creative();
        creative.setPrice(new BigDecimal(1));
        creative.setStatus(CreativeStatus.ACTIVE);
        creatives.add(creative);

        Creative creative1 = new Creative();
        creative1.setPrice(new BigDecimal(1));
        creative1.setStatus(CreativeStatus.ACTIVE);
        creatives.add(creative1);

        campaign.setCreatives(creatives);

        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCampaignWithoutActiveCreatives() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();

        Creative creative = new Creative();
        creative.setStatus(CreativeStatus.INACTIVE);
        creative.setPrice(new BigDecimal(1));
        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCampaignWithCreativeWithoutPrice() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();

        Creative creative = new Creative();
        creative.setStatus(CreativeStatus.ACTIVE);
        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCampaignWithPriceLowerThanCreatives() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();

        List<Creative> creatives = new ArrayList<>();
        Creative creative = new Creative();
        creative.setPrice(new BigDecimal(1));
        creative.setStatus(CreativeStatus.ACTIVE);
        creatives.add(creative);

        Creative creative1 = new Creative();
        creative1.setPrice(new BigDecimal(2));
        creative1.setStatus(CreativeStatus.ACTIVE);
        creatives.add(creative1);

        campaign.setCreatives(creatives);

        campaigns.add(campaign);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Campaign has active creatives with different price", campaign.getId()));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

}