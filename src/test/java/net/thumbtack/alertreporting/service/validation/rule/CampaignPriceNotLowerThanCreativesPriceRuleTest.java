package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CampaignPriceNotLowerThanCreativesPriceRuleTest {
    @Mock
    private CampaignDAO campaignDAO;
    @InjectMocks
    private AbstractRule rule = new CampaignPriceNotLowerThanCreativesPriceRule();

    @Test
    public void validateCampaignWithoutCreatives() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaign.setPrice(new BigDecimal(1));
        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateFineCampaign() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaign.setPrice(new BigDecimal(2));

        Creative creative = new Creative();
        creative.setPrice(new BigDecimal(1));
        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCampaignWithoutPrice() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();

        Creative creative = new Creative();
        creative.setPrice(new BigDecimal(1));
        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCampaignWithCreativeWithoutPrice() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaign.setPrice(new BigDecimal(1));

        Creative creative = new Creative();
        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCampaignWithPriceLowerThanCreatives() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();
        Campaign campaign = new Campaign();
        campaign.setPrice(new BigDecimal(1));

        Creative creative = new Creative();
        creative.setId(1);
        creative.setPrice(new BigDecimal(2));
        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Campaign has price lower than its creative#%d price", creative.getId()));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }
}