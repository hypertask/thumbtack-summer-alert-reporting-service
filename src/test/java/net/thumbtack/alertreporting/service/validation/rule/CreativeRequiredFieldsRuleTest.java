package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CreativeDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.creative.CreativeDimension;
import net.thumbtack.alertreporting.entity.creative.CreativeStatus;
import net.thumbtack.alertreporting.entity.creative.CreativeType;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CreativeRequiredFieldsRuleTest {
    @Mock
    private CreativeDAO creativeDAO;
    @InjectMocks
    private AbstractRule rule = new CreativeRequiredFieldsRule();

    @Test
    public void validateFineCreative() throws Exception {
        List<Creative> creatives = new ArrayList<>();

        Creative creative = new Creative();
        creative.setName("test");
        creative.setStatus(CreativeStatus.ACTIVE);
        creative.setDimensions(new CreativeDimension());
        creative.setType(CreativeType.IMAGE);
        creatives.add(creative);

        List<Report> expectedReports = new ArrayList<>();

        when(creativeDAO.getAll()).thenReturn(creatives);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(creativeDAO, times(1)).getAll();
    }

    @Test
    public void validateCreativeWithNullFields() throws Exception {
        List<Creative> creatives = new ArrayList<>();

        Creative creative = new Creative();
        creative.setCampaign(new Campaign());
        creatives.add(creative);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Creative#%d name is empty", creative.getId()));
        issues.add(new Issue("Creative#%d status is empty", creative.getId()));
        issues.add(new Issue("Creative#%d type is empty", creative.getId()));
        issues.add(new Issue("Creative#%d dimensions is empty", creative.getId()));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(creativeDAO.getAll()).thenReturn(creatives);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(creativeDAO, times(1)).getAll();
    }

    @Test
    public void validateCreativeWithEmptyName() throws Exception {
        List<Creative> creatives = new ArrayList<>();

        Creative creative = new Creative();
        creative.setCampaign(new Campaign());
        creative.setName("");
        creatives.add(creative);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Creative#%d name is empty", creative.getId()));
        issues.add(new Issue("Creative#%d status is empty", creative.getId()));
        issues.add(new Issue("Creative#%d type is empty", creative.getId()));
        issues.add(new Issue("Creative#%d dimensions is empty", creative.getId()));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(creativeDAO.getAll()).thenReturn(creatives);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(creativeDAO, times(1)).getAll();
    }

    @Test
    public void validateCreativeWithInactiveStatus() throws Exception {
        List<Creative> creatives = new ArrayList<>();

        Creative creative = new Creative();
        creative.setCampaign(new Campaign());
        creative.setStatus(CreativeStatus.INACTIVE);
        creatives.add(creative);

        List<Report> expectedReports = new ArrayList<>();

        when(creativeDAO.getAll()).thenReturn(creatives);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(creativeDAO, times(1)).getAll();
    }
}