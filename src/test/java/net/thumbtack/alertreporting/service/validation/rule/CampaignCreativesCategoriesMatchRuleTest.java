package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.category.Category;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CampaignCreativesCategoriesMatchRuleTest {
    @Mock
    private CampaignDAO campaignDAO;
    @InjectMocks
    private AbstractRule rule = new CampaignCreativesCategoriesMatchRule();

    @Test
    public void validateFineCampaign() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();

        Category campaignCategory1 = new Category();
        campaignCategory1.setCode("1.3");
        Category campaignCategory2 = new Category();
        campaignCategory2.setCode("1.4.3");
        campaign.setCategories(Arrays.asList(campaignCategory1, campaignCategory2));

        Creative creative = new Creative();
        creative.setId(0);

        Category creativeCategory1 = new Category();
        creativeCategory1.setCode("1.3.2.4");
        Category creativeCategory2 = new Category();
        creativeCategory2.setCode("1.4.3.1");
        creative.setCategories(Arrays.asList(creativeCategory1, creativeCategory2));

        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();


        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }


    @Test
    public void validateCampaignWithoutCreatives() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();

        Category campaignCategory1 = new Category();
        campaignCategory1.setCode("1.3");
        Category campaignCategory2 = new Category();
        campaignCategory2.setCode("1.4.3");
        campaign.setCategories(Arrays.asList(campaignCategory1, campaignCategory2));

        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();


        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCreativesWithoutCategories() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();

        Category campaignCategory1 = new Category();
        campaignCategory1.setCode("1.3");
        Category campaignCategory2 = new Category();
        campaignCategory2.setCode("1.4.3");
        campaign.setCategories(Arrays.asList(campaignCategory1, campaignCategory2));

        Creative creative = new Creative();
        creative.setId(0);

        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();


        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCamapagnCategoriesNullMismatch() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();

        Creative creative = new Creative();
        creative.setId(0);

        Category creativeCategory1 = new Category();
        creativeCategory1.setCode("1.3.2.4");
        Category creativeCategory2 = new Category();
        creativeCategory2.setCode("1.4.3.1");
        creative.setCategories(Arrays.asList(creativeCategory1, creativeCategory2));

        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Creatives have categories while parent campaign doesn't"));

        List<Report> expectedReports = new ArrayList<>();

        Report report = new Report(null, null, issues, new Date());

        expectedReports.add(report);

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();


        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCampaignCategoriesEmptyMismatch() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();
        campaign.setCategories(new ArrayList<>());

        Creative creative = new Creative();
        creative.setId(0);

        Category creativeCategory1 = new Category();
        creativeCategory1.setCode("1.3.2.4");
        Category creativeCategory2 = new Category();
        creativeCategory2.setCode("1.4.3.1");
        creative.setCategories(Arrays.asList(creativeCategory1, creativeCategory2));

        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Creatives have categories while parent campaign doesn't"));

        List<Report> expectedReports = new ArrayList<>();

        Report report = new Report(null, null, issues, new Date());

        expectedReports.add(report);

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();


        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCategoriesMismatch() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();

        Category campaignCategory1 = new Category();
        campaignCategory1.setCode("1.3");
        Category campaignCategory2 = new Category();
        campaignCategory2.setCode("1.1.3");
        campaign.setCategories(Arrays.asList(campaignCategory1, campaignCategory2));

        Creative creative = new Creative();
        creative.setId(0);

        Category creativeCategory1 = new Category();
        creativeCategory1.setCode("1.3.2.4");
        Category creativeCategory2 = new Category();
        creativeCategory2.setCode("1.4.3.1");
        creative.setCategories(Arrays.asList(creativeCategory1, creativeCategory2));

        campaign.setCreatives(Collections.singletonList(creative));

        campaigns.add(campaign);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Creative#0 categories mismatch"));

        List<Report> expectedReports = new ArrayList<>();

        Report report = new Report(null, null, issues, new Date());

        expectedReports.add(report);

        when(campaignDAO.getAll()).thenReturn(campaigns);

        List<Report> actualReports = rule.validate();


        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

}