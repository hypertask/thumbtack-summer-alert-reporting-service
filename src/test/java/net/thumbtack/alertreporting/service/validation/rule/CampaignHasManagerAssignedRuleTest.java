package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.dao.UserDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import net.thumbtack.alertreporting.entity.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CampaignHasManagerAssignedRuleTest {
    @Mock
    private CampaignDAO campaignDAO;
    @Mock
    private UserDAO userDAO;
    @InjectMocks
    private AbstractRule rule = new CampaignHasManagerAssignedRule();

    @Test
    public void validateFineCampaign() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();
        campaigns.add(campaign);

        List<User> users = Arrays.asList(new User("username", "email", "password",
                "firstName", "lastName", new ArrayList<>()));

        List<Report> expectedReports = new ArrayList<>();

        when(campaignDAO.getAll()).thenReturn(campaigns);
        when(userDAO.getByCampaign(campaign)).thenReturn(users);

        List<Report> actualReports = rule.validate();


        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCampaignHasNoManager() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();
        campaigns.add(campaign);

        List<User> users = new ArrayList<>();

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Unfinished campaign has no manager assigned"));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());

        expectedReports.add(report);

        when(campaignDAO.getAll()).thenReturn(campaigns);
        when(userDAO.getByCampaign(campaign)).thenReturn(users);

        List<Report> actualReports = rule.validate();


        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }

    @Test
    public void validateCampaignHasMoreThanOneManager() throws Exception {
        List<Campaign> campaigns = new ArrayList<>();

        Campaign campaign = new Campaign();
        campaigns.add(campaign);

        List<User> users = Arrays.asList(
                new User("username", "email", "password",
                        "firstName", "lastName", new ArrayList<>()),
                new User("username", "email", "password",
                        "firstName", "lastName", new ArrayList<>()));

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Campaign has more than one manager assigned"));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(campaignDAO.getAll()).thenReturn(campaigns);
        when(userDAO.getByCampaign(campaign)).thenReturn(users);

        List<Report> actualReports = rule.validate();


        assertEquals(expectedReports, actualReports);
        verify(campaignDAO, times(1)).getAll();
    }
}