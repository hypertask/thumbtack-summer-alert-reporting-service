package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CreativeDAO;
import net.thumbtack.alertreporting.entity.advertiser.Advertiser;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CreativeLandingPageRuleTest {
    @Mock
    private CreativeDAO creativeDAO;
    @InjectMocks
    private AbstractRule rule = new CreativeLandingPageRule();

    @Test
    public void validateFineUrls() throws Exception {
        List<Creative> creatives = new ArrayList<>();

        Creative creative = new Creative();
        creative.setLandingPageUrl("test.example.com?rid=test");

        Advertiser advertiser = new Advertiser();
        advertiser.setSite("example.com");
        creative.setAdvertiser(advertiser);

        creatives.add(creative);

        List<Report> expectedReports = new ArrayList<>();

        when(creativeDAO.getAll()).thenReturn(creatives);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(creativeDAO, times(1)).getAll();
    }

    @Test
    public void validateUrlsMismatch() throws Exception {
        List<Creative> creatives = new ArrayList<>();

        Creative creative = new Creative();
        creative.setId(0);
        creative.setLandingPageUrl("example.com?rid=test");

        Advertiser advertiser = new Advertiser();
        advertiser.setSite("test.example.com");
        creative.setAdvertiser(advertiser);

        creatives.add(creative);

        List<Issue> issues = new ArrayList<>();
        issues.add(new Issue("Creative#%d landing page doesn't match its advertiser site domain", creative.getId()));

        List<Report> expectedReports = new ArrayList<>();
        Report report = new Report(null, null, issues, new Date());
        expectedReports.add(report);

        when(creativeDAO.getAll()).thenReturn(creatives);

        List<Report> actualReports = rule.validate();

        assertEquals(expectedReports, actualReports);
        verify(creativeDAO, times(1)).getAll();
    }
}