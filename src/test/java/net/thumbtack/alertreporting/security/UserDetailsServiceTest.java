package net.thumbtack.alertreporting.security;

import net.thumbtack.alertreporting.dao.UserDAO;
import net.thumbtack.alertreporting.entity.user.Role;
import net.thumbtack.alertreporting.entity.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserDetailsServiceTest {
    @Mock
    private UserDAO userDAO;
    @InjectMocks
    private org.springframework.security.core.userdetails.UserDetailsService userDetailsService = new UserDetailsServiceImpl(userDAO);

    @Test
    public void loadUserByUsername() throws Exception {

        List<Role> roles = new ArrayList<>();
        roles.add(new Role("ADMIN"));
        User user = new User("Username", "Email", "Password",
                "FirstName", "LastName", roles);

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getRole())));

        UserDetailsImpl expectedUserDetailsImpl = new UserDetailsImpl();
        expectedUserDetailsImpl.setUsername(user.getUsername());
        expectedUserDetailsImpl.setPassword(user.getPassword());
        expectedUserDetailsImpl.setAuthorities(authorities);
        when(userDAO.getByUsername(user.getUsername())).thenReturn(user);

        org.springframework.security.core.userdetails.UserDetails actualUserDetails = userDetailsService.loadUserByUsername(user.getUsername());

        assertEquals(expectedUserDetailsImpl, actualUserDetails);
        verify(userDAO, times(1)).getByUsername(user.getUsername());
    }

}