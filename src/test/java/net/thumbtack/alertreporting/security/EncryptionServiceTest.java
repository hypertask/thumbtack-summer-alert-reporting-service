package net.thumbtack.alertreporting.security;

import org.jasypt.util.password.PasswordEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class EncryptionServiceTest {
    @Mock
    private PasswordEncryptor encryptor;
    @InjectMocks
    private EncryptionService encryptionService = new EncryptionService();

    @Test
    public void encryptPassword() throws Exception {
        String passwordToEncrypt = "test";
        String expectedPassword = "encryptedTest";
        String actualPassword;

        when(encryptor.encryptPassword(passwordToEncrypt)).thenReturn(expectedPassword);

        actualPassword = encryptionService.encryptPassword(passwordToEncrypt);

        assertEquals(expectedPassword, actualPassword);
        verify(encryptor, times(1)).encryptPassword(passwordToEncrypt);
    }

    @Test
    public void checkPassword() throws Exception {
        String passwordToCheck = "test";

        when(encryptor.checkPassword(passwordToCheck, passwordToCheck)).thenReturn(true);

        boolean passwordsMatch = encryptionService.checkPassword(passwordToCheck, passwordToCheck);

        assertEquals(true, passwordsMatch);
        verify(encryptor, times(1)).checkPassword(passwordToCheck, passwordToCheck);
    }

}