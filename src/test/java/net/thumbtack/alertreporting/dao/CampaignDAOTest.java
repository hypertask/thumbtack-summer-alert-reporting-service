package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.dao.impl.CampaignDAOImpl;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.mapper.CampaignMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CampaignDAOTest {


    @Mock
    private CampaignMapper campaignMapper;
    @InjectMocks
    private CampaignDAO campaignDAO = new CampaignDAOImpl(campaignMapper);

    @Test
    public void getAll() throws Exception {
        List<Campaign> expectedCampaigns = new ArrayList<>();
        expectedCampaigns.add(new Campaign());

        when(campaignMapper.getAll()).thenReturn(expectedCampaigns);

        List<Campaign> actualCampaigns = campaignDAO.getAll();

        assertNotNull(actualCampaigns);
        assertEquals(expectedCampaigns, actualCampaigns);
        verify(campaignMapper, times(1)).getAll();
    }

    @Test
    public void getByArchivedStatus() throws Exception {
        List<Campaign> expectedCampaigns = new ArrayList<>();

        Campaign campaign = new Campaign();
        campaign.setArchived(true);

        expectedCampaigns.add(campaign);

        when(campaignMapper.getByArchivedStatus(true)).thenReturn(expectedCampaigns);

        List<Campaign> actualCampaigns = campaignDAO.getByArchivedStatus(true);

        assertNotNull(actualCampaigns);
        assertEquals(expectedCampaigns, actualCampaigns);
        verify(campaignMapper, timeout(1)).getByArchivedStatus(true);
    }

    @Test
    public void getById() throws Exception {
        Campaign expectedCampaign = new Campaign();

        when(campaignMapper.getById(0)).thenReturn(expectedCampaign);

        Campaign actualCampaign = campaignDAO.getById(0);

        assertNotNull(actualCampaign);
        assertEquals(expectedCampaign, actualCampaign);

        verify(campaignMapper, timeout(1)).getById(0);
    }

    @Test
    public void getNotAssigned() throws Exception {
        List<Campaign> expectedCampaigns = Collections.singletonList(new Campaign());

        when(campaignMapper.getNotAssigned()).thenReturn(expectedCampaigns);

        List<Campaign> actualCampaigns = campaignDAO.getNotAssigned();

        assertNotNull(actualCampaigns);
        assertEquals(expectedCampaigns, actualCampaigns);
        verify(campaignMapper, timeout(1)).getNotAssigned();
    }

    @Test
    public void getByUserId() throws Exception {
        List<Campaign> expectedCampaigns = Collections.singletonList(new Campaign());

        when(campaignMapper.getByUserId(0)).thenReturn(expectedCampaigns);

        List<Campaign> actualCampaigns = campaignDAO.getByUserId(0);

        assertNotNull(actualCampaigns);
        assertEquals(expectedCampaigns, actualCampaigns);
        verify(campaignMapper, timeout(1)).getByUserId(0);
    }
}