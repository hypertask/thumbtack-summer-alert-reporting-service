package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.dao.impl.UserDAOImpl;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.user.Role;
import net.thumbtack.alertreporting.entity.user.User;
import net.thumbtack.alertreporting.mapper.RoleMapper;
import net.thumbtack.alertreporting.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserDAOTest {
    @Mock
    private UserMapper userMapper;
    @Mock
    private RoleMapper roleMapper;
    @InjectMocks
    private UserDAO userDAO = new UserDAOImpl(userMapper, roleMapper);

    @Test
    public void insert() throws Exception {
        Role role = new Role("USER");
        User expectedUser = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));

        when(userMapper.insert(expectedUser)).thenReturn(null);
        when(userMapper.addRole(expectedUser, role)).thenReturn(null);

        User actualUser = userDAO.insert(expectedUser);

        assertNotNull(actualUser);
        assertEquals(expectedUser, actualUser);
        verify(userMapper, times(1)).insert(expectedUser);
        verify(userMapper, times(1)).addRole(expectedUser, role);
    }

    @Test
    public void update() throws Exception {
        Role role = new Role("USER");
        User expectedUser = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));

        when(userMapper.update(expectedUser)).thenReturn(null);
        when(userMapper.deleteRoles(expectedUser)).thenReturn(null);
        when(userMapper.addRole(expectedUser, role)).thenReturn(null);

        userDAO.update(expectedUser);

        verify(userMapper, times(1)).update(expectedUser);
        verify(userMapper, times(1)).deleteRoles(expectedUser);
        verify(userMapper, times(1)).addRole(expectedUser, role);
    }

    @Test
    public void delete() throws Exception {
        Role role = new Role("USER");
        User expectedUser = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));

        when(userMapper.delete(expectedUser.getId())).thenReturn(null);

        userDAO.delete(expectedUser.getId());

        verify(userMapper, times(1)).delete(expectedUser.getId());
    }

    @Test
    public void getById() throws Exception {
        Role role = new Role("USER");
        User expectedUser = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));
        expectedUser.setId(0);

        when(userMapper.getById(expectedUser.getId())).thenReturn(expectedUser);

        User actualUser = userDAO.getById(expectedUser.getId());

        assertNotNull(actualUser);
        assertEquals(expectedUser, actualUser);
        verify(userMapper, times(1)).getById(expectedUser.getId());
    }

    @Test
    public void getByUsername() throws Exception {
        Role role = new Role("USER");
        User expectedUser = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));

        when(userMapper.getByUsername(expectedUser.getUsername())).thenReturn(expectedUser);

        User actualUser = userDAO.getByUsername(expectedUser.getUsername());

        assertNotNull(actualUser);
        assertEquals(expectedUser, actualUser);
        verify(userMapper, times(1)).getByUsername(expectedUser.getUsername());
    }

    @Test
    public void getAll() throws Exception {
        Role role = new Role("USER");
        User user = new User("username", "email", "password",
                "firstName", "lastName", Collections.singletonList(role));
        List<User> expectedUsers = Collections.singletonList(user);

        when(userMapper.getAll()).thenReturn(expectedUsers);

        List<User> actualUsers = userDAO.getAll();

        assertNotNull(actualUsers);
        assertEquals(expectedUsers, actualUsers);
        verify(userMapper, times(1)).getAll();
    }

    @Test
    public void addCampaign() throws Exception {
        when(userMapper.addCampaign(0, 0)).thenReturn(null);

        userDAO.addCampaign(0, 0);

        verify(userMapper, times(1)).addCampaign(0, 0);
    }

    @Test
    public void removeCampaign() throws Exception {
        when(userMapper.removeCampaign(0, 0)).thenReturn(null);

        userDAO.removeCampaign(0, 0);

        verify(userMapper, times(1)).removeCampaign(0, 0);
    }

    @Test
    public void getByCampaign() throws Exception {
        Campaign campaign = new Campaign();

        List<User> expectedUsers = Collections.singletonList(new User());

        when(userMapper.getByCampaign(campaign)).thenReturn(expectedUsers);

        List<User> actualUsers = userDAO.getByCampaign(campaign);

        assertNotNull(actualUsers);
        assertEquals(expectedUsers, actualUsers);
        verify(userMapper, times(1)).getByCampaign(campaign);
    }

    @Test
    public void getAllRoles() throws Exception {
        List<Role> expectedRoles = Collections.singletonList(new Role("ADMIN"));

        when(roleMapper.getAll()).thenReturn(expectedRoles);

        List<Role> actualRoles = userDAO.getAllRoles();

        assertNotNull(actualRoles);
        assertEquals(expectedRoles, actualRoles);
        verify(roleMapper, times(1)).getAll();
    }
}