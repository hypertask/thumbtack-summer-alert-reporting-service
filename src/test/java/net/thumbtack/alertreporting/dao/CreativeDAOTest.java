package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.dao.impl.CreativeDAOImpl;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.mapper.CreativeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CreativeDAOTest {
    @Mock
    private CreativeMapper creativeMapper;
    @InjectMocks
    private CreativeDAO creativeDAO = new CreativeDAOImpl(creativeMapper);

    @Test
    public void getAll() throws Exception {
        List<Creative> expectedCreatives = new ArrayList<>();
        expectedCreatives.add(new Creative());

        when(creativeMapper.getAll()).thenReturn(expectedCreatives);

        List<Creative> actualReports = creativeDAO.getAll();

        assertNotNull(actualReports);
        assertEquals(expectedCreatives, actualReports);
        verify(creativeMapper, times(1)).getAll();
    }
}