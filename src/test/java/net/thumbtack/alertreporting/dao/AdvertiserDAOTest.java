package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.dao.impl.AdvertiserDAOImpl;
import net.thumbtack.alertreporting.entity.advertiser.Advertiser;
import net.thumbtack.alertreporting.mapper.AdvertiserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class AdvertiserDAOTest {
    @Mock
    private AdvertiserMapper advertiserMapper;
    @InjectMocks
    private AdvertiserDAO advertiserDAO = new AdvertiserDAOImpl(advertiserMapper);

    @Test
    public void getAll() throws Exception {
        List<Advertiser> expectedAdvertisers = new ArrayList<>();
        expectedAdvertisers.add(new Advertiser());

        when(advertiserMapper.getAll()).thenReturn(expectedAdvertisers);

        List<Advertiser> actualReports = advertiserDAO.getAll();

        assertNotNull(actualReports);
        assertEquals(expectedAdvertisers, actualReports);
        verify(advertiserMapper, times(1)).getAll();
    }

}