package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.dao.impl.ReportDAOImpl;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import net.thumbtack.alertreporting.mapper.IssueMapper;
import net.thumbtack.alertreporting.mapper.ReportMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class ReportDAOTest {
    @Mock
    private ReportMapper reportMapper;
    @Mock
    private IssueMapper issueMapper;
    @InjectMocks
    private ReportDAO reportDAO = new ReportDAOImpl(reportMapper, issueMapper);

    @Test
    public void insert() throws Exception {
        Report expectedReport = new Report();
        Issue issue = new Issue("Test issue");
        expectedReport.setIssues(Collections.singletonList(issue));

        when(issueMapper.insert(issue)).thenReturn(null);
        when(reportMapper.insert(expectedReport)).thenReturn(null);
        when(reportMapper.addIssue(expectedReport, issue)).thenReturn(null);

        Report actualReport = reportDAO.insert(expectedReport);

        assertNotNull(actualReport);
        assertEquals(expectedReport, actualReport);
        verify(issueMapper, times(1)).insert(issue);
        verify(reportMapper, times(1)).insert(expectedReport);
        verify(reportMapper, times(1)).addIssue(expectedReport, issue);
    }

    @Test
    public void getAll() throws Exception {
        List<Report> expectedReports = new ArrayList<>();
        expectedReports.add(new Report());

        when(reportMapper.getAll()).thenReturn(expectedReports);

        List<Report> actualReports = reportDAO.getAll();

        assertNotNull(actualReports);
        assertEquals(expectedReports, actualReports);
        verify(reportMapper, times(1)).getAll();
    }

    @Test
    public void getUserReports() throws Exception {
        List<Report> expectedReports = new ArrayList<>();
        expectedReports.add(new Report());

        when(reportMapper.getUserReports("user")).thenReturn(expectedReports);

        List<Report> actualReports = reportDAO.getUserReports("user");

        assertNotNull(actualReports);
        assertEquals(expectedReports, actualReports);
        verify(reportMapper, times(1)).getUserReports("user");
    }
}