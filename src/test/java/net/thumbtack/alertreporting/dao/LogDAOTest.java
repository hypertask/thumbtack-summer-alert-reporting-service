package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.dao.impl.LogDAOImpl;
import net.thumbtack.alertreporting.entity.logmessage.LogMessage;
import net.thumbtack.alertreporting.mapper.LogMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class LogDAOTest {
    @Mock
    private LogMapper logMapper;
    @InjectMocks
    private LogDAO logDAO = new LogDAOImpl(logMapper);

    @Test
    public void getAll() throws Exception {
        List<LogMessage> expectedMessages = Arrays.asList(new LogMessage(null, null, null));

        when(logMapper.getAll()).thenReturn(expectedMessages);

        List<LogMessage> actualMessages = logDAO.getAll();

        assertNotNull(actualMessages);
        assertEquals(expectedMessages, actualMessages);
        verify(logMapper, times(1)).getAll();
    }

}