package net.thumbtack.alertreporting.controller;

import net.thumbtack.alertreporting.entity.user.User;
import net.thumbtack.alertreporting.security.JwtTokenUtil;
import net.thumbtack.alertreporting.security.UserDetailsImpl;
import net.thumbtack.alertreporting.security.UserDetailsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class AuthenticationControllerTest {
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private UserDetailsServiceImpl userDetailsService;
    @Mock
    private JwtTokenUtil jwtTokenUtil;
    @InjectMocks
    private AuthenticationController authenticationController
            = new AuthenticationController(authenticationManager, userDetailsService);

    @Test
    public void createAuthenticationToken() throws Exception {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");

        UsernamePasswordAuthenticationToken authenticationToken
                = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());

        UserDetails userDetails = new UserDetailsImpl();

        String token = "test-token";

        ResponseEntity expectedResponse = new ResponseEntity(token, HttpStatus.OK);

        when(authenticationManager.authenticate(authenticationToken)).thenReturn(null);
        when(userDetailsService.loadUserByUsername(user.getUsername())).thenReturn(userDetails);
        when(jwtTokenUtil.generateToken(userDetails)).thenReturn(token);

        ResponseEntity actualResponse = authenticationController.createAuthenticationToken(user);

        assertNotNull(actualResponse);
        assertEquals(expectedResponse, actualResponse);
        verify(authenticationManager, times(1)).authenticate(authenticationToken);
        verify(userDetailsService, times(1)).loadUserByUsername(user.getUsername());
        verify(jwtTokenUtil, times(1)).generateToken(userDetails);
    }
}