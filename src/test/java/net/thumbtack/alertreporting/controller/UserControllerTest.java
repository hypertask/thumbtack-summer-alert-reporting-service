package net.thumbtack.alertreporting.controller;

import net.thumbtack.alertreporting.entity.user.Role;
import net.thumbtack.alertreporting.entity.user.User;
import net.thumbtack.alertreporting.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {

    @Mock
    private UserService userService;
    @InjectMocks
    private UserController userController = new UserController(userService);

    @Test
    public void add() throws Exception {
        User expectedUser = new User();

        when(userService.addUser(expectedUser)).thenReturn(expectedUser);

        User actualUser = userController.add(expectedUser);

        assertNotNull(actualUser);
        assertEquals(expectedUser, actualUser);
        verify(userService, times(1)).addUser(expectedUser);
    }

    @Test
    public void update() throws Exception {
        User expectedUser = new User();

        doNothing().when(userService).update(expectedUser);

        userController.update(0, expectedUser);

        verify(userService, times(1)).update(expectedUser);
    }

    @Test
    public void assign() throws Exception {
        doNothing().when(userService).assign(0, 0);

        userController.assign(0, 0);

        verify(userService, times(1)).assign(0, 0);
    }

    @Test
    public void unassign() throws Exception {
        doNothing().when(userService).unassign(0, 0);

        userController.unassign(0, 0);

        verify(userService, times(1)).unassign(0, 0);
    }

    @Test
    public void delete() throws Exception {
        doNothing().when(userService).delete(0);

        userController.delete(0);

        verify(userService, times(1)).delete(0);
    }

    @Test
    public void get() throws Exception {
        User expectedUser = new User();

        when(userService.get(0)).thenReturn(expectedUser);

        User actualUser = userController.get(0);

        assertNotNull(actualUser);
        assertEquals(expectedUser, actualUser);
        verify(userService, times(1)).get(0);
    }

    @Test
    public void getAll() throws Exception {
        User user = new User();
        List<User> expectedUsers = Collections.singletonList(user);

        when(userService.getAll()).thenReturn(expectedUsers);

        List<User> actualUsers = userController.getAll();

        assertNotNull(actualUsers);
        assertEquals(expectedUsers, actualUsers);
        verify(userService, times(1)).getAll();
    }

    @Test
    public void getAllRoles() throws Exception {
        List<Role> expectedRoles = Collections.singletonList(new Role());

        when(userService.getAllRoles()).thenReturn(expectedRoles);

        List<Role> actualRoles = userController.getAllRoles();

        assertNotNull(actualRoles);
        assertEquals(expectedRoles, actualRoles);
        verify(userService, times(1)).getAllRoles();
    }
}