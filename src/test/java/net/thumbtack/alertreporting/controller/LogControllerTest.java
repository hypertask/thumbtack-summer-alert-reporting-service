package net.thumbtack.alertreporting.controller;

import net.thumbtack.alertreporting.entity.logmessage.LogMessage;
import net.thumbtack.alertreporting.service.LogService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class LogControllerTest {
    @Mock
    private LogService logService;
    @InjectMocks
    private LogController logController;

    @Test
    public void getAll() throws Exception {
        List<LogMessage> expectedMessages = Arrays.asList(new LogMessage(null, null, null));

        when(logService.getAll()).thenReturn(expectedMessages);

        List<LogMessage> actualMessages = logController.getAll();

        assertNotNull(actualMessages);
        assertEquals(expectedMessages, actualMessages);
        verify(logService, times(1)).getAll();
    }
}