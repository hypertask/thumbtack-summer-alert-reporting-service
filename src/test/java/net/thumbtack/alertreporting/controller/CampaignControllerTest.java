package net.thumbtack.alertreporting.controller;

import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.service.CampaignService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CampaignControllerTest {

    @Mock
    private CampaignService campaignService;
    @InjectMocks
    CampaignController campaignController = new CampaignController(campaignService);

    @Test
    public void getUserCampaigns() throws Exception {
        List<Campaign> expectedCampaigns = Collections.singletonList(new Campaign());

        when(campaignService.getUserCampaigns(0)).thenReturn(expectedCampaigns);

        List<Campaign> actualCampaigns = campaignController.getUserCampaigns(0);

        assertNotNull(actualCampaigns);
        assertEquals(expectedCampaigns, actualCampaigns);
        verify(campaignService, times(1)).getUserCampaigns(0);
    }

    @Test
    public void getCampaignsWithoutManage() throws Exception {
        List<Campaign> expectedCampaigns = Collections.singletonList(new Campaign());

        when(campaignService.getNotAssigned()).thenReturn(expectedCampaigns);

        List<Campaign> actualCampaigns = campaignController.getCampaignsWithoutManage();

        assertNotNull(actualCampaigns);
        assertEquals(expectedCampaigns, actualCampaigns);
        verify(campaignService, times(1)).getNotAssigned();

    }

}