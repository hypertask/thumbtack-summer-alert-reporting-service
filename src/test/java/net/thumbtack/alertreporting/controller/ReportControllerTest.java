package net.thumbtack.alertreporting.controller;

import net.thumbtack.alertreporting.entity.report.Report;
import net.thumbtack.alertreporting.service.ReportService;
import net.thumbtack.alertreporting.service.validation.rule.RuleInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class ReportControllerTest {
    @Mock
    private ReportService reportService;
    @InjectMocks
    private ReportController reportController = new ReportController(reportService);

    @Test
    public void validate() throws Exception {
        List<String> rules = Collections.singletonList("test");
        List<Report> expectedReports = Collections.singletonList(new Report());

        when(reportService.validate(rules)).thenReturn(expectedReports);

        List<Report> actualReports = reportController.validate(rules);

        assertEquals(expectedReports, actualReports);
        verify(reportService, times(1)).validate(rules);
    }

    @Test
    public void getAll() throws Exception {
        List<Report> expectedReports = new ArrayList<>();
        expectedReports.add(new Report());

        when(reportService.getUserReports()).thenReturn(expectedReports);

        List<Report> actualReports = reportController.getUserReports();

        assertNotNull(actualReports);
        assertEquals(expectedReports, actualReports);
        verify(reportService, times(1)).getUserReports();
    }


    @Test
    public void getUserReports() throws Exception {
        List<Report> expectedReports = new ArrayList<>();
        expectedReports.add(new Report());

        when(reportService.getUserReports()).thenReturn(expectedReports);

        List<Report> actualReports = reportController.getUserReports();

        assertNotNull(actualReports);
        assertEquals(expectedReports, actualReports);
        verify(reportService, times(1)).getUserReports();
    }

    @Test
    public void getRules() throws Exception {
        RuleInfo[] expectedRulesInfo = RuleInfo.values();

        when(reportService.getRules()).thenReturn(expectedRulesInfo);

        RuleInfo[] actualRulesInfo = reportController.getRules();

        assertNotNull(actualRulesInfo);
        assertArrayEquals(expectedRulesInfo, actualRulesInfo);
        verify(reportService, times(1)).getRules();
    }
}