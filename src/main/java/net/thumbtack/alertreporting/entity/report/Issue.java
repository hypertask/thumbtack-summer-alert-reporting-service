package net.thumbtack.alertreporting.entity.report;

public class Issue {
    private Integer id;
    private String issue;

    public Issue() {

    }

    public Issue(String issue) {
        this.issue = issue;
    }

    public Issue(String issue, Integer id) {
        this.issue = String.format(issue, id);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Issue that = (Issue) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return issue != null ? issue.equals(that.issue) : that.issue == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (issue != null ? issue.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Issue{" +
                "id=" + id +
                ", issue='" + issue + '\'' +
                '}';
    }
}
