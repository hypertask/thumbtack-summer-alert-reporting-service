package net.thumbtack.alertreporting.entity.report;

import java.util.Date;
import java.util.List;

public class Report {
    private Integer id;
    private Integer campaignId;
    private String campaignName;
    private Date created;

    private List<Issue> issues;

    public Report() {

    }

    public Report(List<Issue> issues, Date created) {
        this.issues = issues;
        this.created = created;
    }

    public Report(Integer campaignId, String campaignName, List<Issue> issues, Date created) {
        this.campaignId = campaignId;
        this.campaignName = campaignName;
        this.issues = issues;
        this.created = created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public void setIssues(List<Issue> issues) {
        this.issues = issues;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public Report merge(Report report) {
        issues.addAll(report.getIssues());
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Report report = (Report) o;

        if (id != null ? !id.equals(report.id) : report.id != null) return false;
        if (campaignId != null ? !campaignId.equals(report.campaignId) : report.campaignId != null) return false;
        if (campaignName != null ? !campaignName.equals(report.campaignName) : report.campaignName != null)
            return false;
        return issues != null ? issues.equals(report.issues) : report.issues == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (campaignId != null ? campaignId.hashCode() : 0);
        result = 31 * result + (campaignName != null ? campaignName.hashCode() : 0);
        result = 31 * result + (issues != null ? issues.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Report{" +
                "id=" + id +
                ", campaignId=" + campaignId +
                ", campaignName='" + campaignName + '\'' +
                ", created=" + created +
                ", issues=" + issues +
                '}';
    }
}
