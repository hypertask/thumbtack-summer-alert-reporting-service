package net.thumbtack.alertreporting.entity.campaign;

import net.thumbtack.alertreporting.entity.advertiser.Advertiser;
import net.thumbtack.alertreporting.entity.category.Category;
import net.thumbtack.alertreporting.entity.creative.Creative;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Campaign {
    private Integer id;
    private Advertiser advertiser;
    private CampaignStatus status;
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private Integer impressionCap;
    private BigDecimal budget;
    private BigDecimal price;
    private Boolean archived;
    private List<Creative> creatives;
    private List<Category> categories;

    public Campaign() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Advertiser getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(Advertiser advertiser) {
        this.advertiser = advertiser;
    }

    public CampaignStatus getStatus() {
        return status;
    }

    public void setStatus(CampaignStatus status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getImpressionCap() {
        return impressionCap;
    }

    public void setImpressionCap(Integer impressionCap) {
        this.impressionCap = impressionCap;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    public void setBudget(BigDecimal budget) {
        this.budget = budget;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getArchived() {
        return archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public List<Creative> getCreatives() {
        return creatives;
    }

    public void setCreatives(List<Creative> creatives) {
        this.creatives = creatives;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Campaign campaign = (Campaign) o;

        if (id != null ? !id.equals(campaign.id) : campaign.id != null) return false;
        if (advertiser != null ? !advertiser.equals(campaign.advertiser) : campaign.advertiser != null) return false;
        if (status != campaign.status) return false;
        if (name != null ? !name.equals(campaign.name) : campaign.name != null) return false;
        if (description != null ? !description.equals(campaign.description) : campaign.description != null)
            return false;
        if (startDate != null ? !startDate.equals(campaign.startDate) : campaign.startDate != null) return false;
        if (endDate != null ? !endDate.equals(campaign.endDate) : campaign.endDate != null) return false;
        if (impressionCap != null ? !impressionCap.equals(campaign.impressionCap) : campaign.impressionCap != null)
            return false;
        if (budget != null ? !budget.equals(campaign.budget) : campaign.budget != null) return false;
        if (price != null ? !price.equals(campaign.price) : campaign.price != null) return false;
        if (archived != null ? !archived.equals(campaign.archived) : campaign.archived != null) return false;
        if (creatives != null ? !creatives.equals(campaign.creatives) : campaign.creatives != null) return false;
        return categories != null ? categories.equals(campaign.categories) : campaign.categories == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (advertiser != null ? advertiser.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (impressionCap != null ? impressionCap.hashCode() : 0);
        result = 31 * result + (budget != null ? budget.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (archived != null ? archived.hashCode() : 0);
        result = 31 * result + (creatives != null ? creatives.hashCode() : 0);
        result = 31 * result + (categories != null ? categories.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Campaign{" +
                "id=" + id +
                ", advertiser=" + advertiser +
                ", status=" + status +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", impressionCap=" + impressionCap +
                ", budget=" + budget +
                ", price=" + price +
                ", archived=" + archived +
                ", creatives=" + creatives +
                ", categories=" + categories +
                '}';
    }
}
