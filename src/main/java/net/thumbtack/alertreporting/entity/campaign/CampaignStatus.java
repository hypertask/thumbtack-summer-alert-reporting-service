package net.thumbtack.alertreporting.entity.campaign;

public enum CampaignStatus {
    ACTIVE("Active"),
    DRAFT("Draft"),
    PAUSED("Paused"),
    ENDED("Ended");

    private String status;

    CampaignStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
