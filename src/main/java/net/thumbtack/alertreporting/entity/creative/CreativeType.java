package net.thumbtack.alertreporting.entity.creative;

public enum CreativeType {
    IMAGE("Image"),
    VIDEO("Video");

    private String type;

    CreativeType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
