package net.thumbtack.alertreporting.entity.creative;

public enum CreativeStatus {
    ACTIVE("Active"),
    INACTIVE("Inactive");

    private String status;

    CreativeStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
