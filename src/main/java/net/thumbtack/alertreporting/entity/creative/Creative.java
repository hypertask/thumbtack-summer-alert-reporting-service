package net.thumbtack.alertreporting.entity.creative;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.thumbtack.alertreporting.entity.advertiser.Advertiser;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.category.Category;

import java.math.BigDecimal;
import java.util.List;

public class Creative {
    private Integer id;
    private Campaign campaign;
    private Advertiser advertiser;
    private String name;
    private String description;
    private String landingPageUrl;
    private CreativeStatus status;
    private CreativeType type;
    private CreativeDimension dimensions;
    private BigDecimal price;
    private List<Category> categories;

    public Creative() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    @JsonIgnore
    public Integer getCampaignId() {
        return campaign.getId();
    }

    @JsonIgnore
    public String getCampaignName() {
        return campaign.getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLandingPageUrl() {
        return landingPageUrl;
    }

    public void setLandingPageUrl(String landingPageUrl) {
        this.landingPageUrl = landingPageUrl;
    }

    public CreativeStatus getStatus() {
        return status;
    }

    public void setStatus(CreativeStatus status) {
        this.status = status;
    }

    public CreativeType getType() {
        return type;
    }

    public void setType(CreativeType type) {
        this.type = type;
    }

    public CreativeDimension getDimensions() {
        return dimensions;
    }

    public void setDimensions(CreativeDimension dimensions) {
        this.dimensions = dimensions;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Advertiser getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(Advertiser advertiser) {
        this.advertiser = advertiser;
    }

    @JsonIgnore
    public String getAdvertisersSite() {
        return this.advertiser.getSite();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Creative creative = (Creative) o;

        if (id != null ? !id.equals(creative.id) : creative.id != null) return false;
        if (campaign != null ? !campaign.equals(creative.campaign) : creative.campaign != null) return false;
        if (advertiser != null ? !advertiser.equals(creative.advertiser) : creative.advertiser != null) return false;
        if (name != null ? !name.equals(creative.name) : creative.name != null) return false;
        if (description != null ? !description.equals(creative.description) : creative.description != null)
            return false;
        if (landingPageUrl != null ? !landingPageUrl.equals(creative.landingPageUrl) : creative.landingPageUrl != null)
            return false;
        if (status != creative.status) return false;
        if (type != creative.type) return false;
        if (dimensions != null ? !dimensions.equals(creative.dimensions) : creative.dimensions != null) return false;
        if (price != null ? !price.equals(creative.price) : creative.price != null) return false;
        return categories != null ? categories.equals(creative.categories) : creative.categories == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (campaign != null ? campaign.hashCode() : 0);
        result = 31 * result + (advertiser != null ? advertiser.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (landingPageUrl != null ? landingPageUrl.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (dimensions != null ? dimensions.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (categories != null ? categories.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Creative{" +
                "id=" + id +
                ", campaign=" + campaign +
                ", advertiser=" + advertiser +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", landingPageUrl='" + landingPageUrl + '\'' +
                ", status=" + status +
                ", type=" + type +
                ", dimensions=" + dimensions +
                ", price=" + price +
                ", categories=" + categories +
                '}';
    }
}
