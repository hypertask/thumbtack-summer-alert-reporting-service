package net.thumbtack.alertreporting.dao.impl;

import net.thumbtack.alertreporting.dao.CreativeDAO;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.mapper.CreativeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CreativeDAOImpl implements CreativeDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreativeDAOImpl.class);
    private CreativeMapper creativeMapper;

    @Autowired
    public CreativeDAOImpl(CreativeMapper creativeMapper) {
        this.creativeMapper = creativeMapper;
    }

    @Override
    public List<Creative> getAll() {
        LOGGER.debug("Get all creatives");
        return creativeMapper.getAll();
    }
}
