package net.thumbtack.alertreporting.dao.impl;

import net.thumbtack.alertreporting.entity.logmessage.LogMessage;
import net.thumbtack.alertreporting.mapper.LogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class BaseDaoImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseDaoImpl.class);
    private LogMapper logMapper;

    @Autowired
    public void setLogMapper(LogMapper logMapper) {
        this.logMapper = logMapper;
    }

    public void writeLog(String action) {
        if (SecurityContextHolder.getContext().getAuthentication() != null) {
            String username = SecurityContextHolder
                    .getContext().getAuthentication().getName();

            LogMessage logMessage = new LogMessage(username, action, new Date());
            LOGGER.debug("Write log: {}", logMessage);
            logMapper.insert(logMessage);
        }
    }
}
