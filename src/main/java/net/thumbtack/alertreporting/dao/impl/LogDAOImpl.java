package net.thumbtack.alertreporting.dao.impl;

import net.thumbtack.alertreporting.dao.LogDAO;
import net.thumbtack.alertreporting.entity.logmessage.LogMessage;
import net.thumbtack.alertreporting.mapper.LogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LogDAOImpl implements LogDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogDAOImpl.class);
    private LogMapper logMapper;

    @Autowired
    public LogDAOImpl(LogMapper logMapper) {
        this.logMapper = logMapper;
    }

    @Override
    public List<LogMessage> getAll() {
        LOGGER.debug("Get all log messages");
        return logMapper.getAll();
    }
}
