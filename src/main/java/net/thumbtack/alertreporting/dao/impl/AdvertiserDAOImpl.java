package net.thumbtack.alertreporting.dao.impl;

import net.thumbtack.alertreporting.dao.AdvertiserDAO;
import net.thumbtack.alertreporting.entity.advertiser.Advertiser;
import net.thumbtack.alertreporting.mapper.AdvertiserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdvertiserDAOImpl implements AdvertiserDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdvertiserDAOImpl.class);
    private AdvertiserMapper advertiserMapper;

    @Autowired
    public AdvertiserDAOImpl(AdvertiserMapper advertiserMapper) {
        this.advertiserMapper = advertiserMapper;
    }

    @Override
    public List<Advertiser> getAll() {
        LOGGER.debug("Get all advertisers");
        return advertiserMapper.getAll();
    }
}
