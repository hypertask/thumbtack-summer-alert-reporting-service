package net.thumbtack.alertreporting.dao.impl;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.mapper.CampaignMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CampaignDAOImpl implements CampaignDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignDAOImpl.class);
    private CampaignMapper campaignMapper;

    @Autowired
    public CampaignDAOImpl(CampaignMapper campaignMapper) {
        this.campaignMapper = campaignMapper;
    }

    @Override
    public List<Campaign> getByArchivedStatus(Boolean archived) {
        LOGGER.debug("Get campaigns with archived status: ", archived);
        return campaignMapper.getByArchivedStatus(archived);
    }

    public Campaign getById(Integer id) {
        LOGGER.debug("Get campaign by id: {}", id);
        return campaignMapper.getById(id);
    }

    @Override
    public List<Campaign> getNotAssigned() {
        LOGGER.debug("Get campaigns without manager assigned");
        return campaignMapper.getNotAssigned();
    }

    @Override
    public List<Campaign> getByUserId(Integer userId) {
        LOGGER.debug("Get campaigns by user id: {}", userId);
        return campaignMapper.getByUserId(userId);
    }

    @Override
    public List<Campaign> getAll() {
        LOGGER.debug("Get all campaigns");
        return campaignMapper.getAll();
    }
}
