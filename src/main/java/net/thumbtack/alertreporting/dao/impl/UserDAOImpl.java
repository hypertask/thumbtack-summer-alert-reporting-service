package net.thumbtack.alertreporting.dao.impl;

import net.thumbtack.alertreporting.dao.UserDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.user.Role;
import net.thumbtack.alertreporting.entity.user.User;
import net.thumbtack.alertreporting.mapper.RoleMapper;
import net.thumbtack.alertreporting.mapper.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDAOImpl extends BaseDaoImpl implements UserDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDAOImpl.class);
    private UserMapper userMapper;
    private RoleMapper roleMapper;

    @Autowired
    public UserDAOImpl(UserMapper userMapper, RoleMapper roleMapper) {
        this.userMapper = userMapper;
        this.roleMapper = roleMapper;
    }

    @Override
    public User insert(User user) {
        LOGGER.debug("Insert user: {}", user);
        userMapper.insert(user);
        writeLog("Insert user" + user.getId());

        List<Role> roles = user.getRoles();
        for (Role role : roles) {
            LOGGER.debug("Add role {} to user {}", role, user);
            userMapper.addRole(user, role);
            writeLog("Add role " + role.getRole() + " to user#" + user.getId());
        }
        return user;
    }

    @Override
    public Integer update(User user) {
        LOGGER.debug("Update user: {}", user);
        userMapper.deleteRoles(user);
        writeLog("Update user#" + user.getId());

        List<Role> roles = user.getRoles();
        for (Role role : roles) {
            LOGGER.debug("Add role {} to user {}", role, user);
            userMapper.addRole(user, role);
            writeLog("Add role " + role.getRole() + " to user#" + user.getId());
        }
        return userMapper.update(user);
    }

    @Override
    public Integer addCampaign(Integer userId, Integer campaignId) {
        LOGGER.debug("Assign user with id {} to campaign with id {}", userId, campaignId);
        writeLog("Assign user#" + userId + " to campaign#" + campaignId);
        return userMapper.addCampaign(userId, campaignId);
    }

    @Override
    public Integer removeCampaign(Integer userId, Integer campaignId) {
        LOGGER.debug("Unassign user with id {} from campaign with id {}", userId, campaignId);
        writeLog("Unassign user#" + userId + " from campaign#" + campaignId);
        return userMapper.removeCampaign(userId, campaignId);
    }

    @Override
    public Integer delete(Integer userId) {
        LOGGER.debug("Delete user with id: {}", userId);
        writeLog("Delete user#" + userId);
        return userMapper.delete(userId);
    }

    @Override
    public User getById(Integer id) {
        LOGGER.debug("Get user by id: {}", id);
        return userMapper.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        LOGGER.debug("Get user by username: {}", username);
        return userMapper.getByUsername(username);
    }

    @Override
    public List<User> getByCampaign(Campaign campaign) {
        LOGGER.debug("Get users by campaign: {}", campaign);
        return userMapper.getByCampaign(campaign);
    }

    @Override
    public List<User> getAll() {
        LOGGER.debug("Get all users");
        return userMapper.getAll();
    }

    @Override
    public List<Role> getAllRoles() {
        LOGGER.debug("Get all roles");
        return roleMapper.getAll();
    }
}
