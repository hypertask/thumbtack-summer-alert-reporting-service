package net.thumbtack.alertreporting.dao.impl;

import net.thumbtack.alertreporting.dao.ReportDAO;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import net.thumbtack.alertreporting.mapper.IssueMapper;
import net.thumbtack.alertreporting.mapper.ReportMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReportDAOImpl extends BaseDaoImpl implements ReportDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportDAOImpl.class);
    private ReportMapper reportMapper;
    private IssueMapper issueMapper;

    @Autowired
    public ReportDAOImpl(ReportMapper reportMapper, IssueMapper issueMapper) {
        this.reportMapper = reportMapper;
        this.issueMapper = issueMapper;
    }

    @Override
    public Report insert(Report report) {
        LOGGER.debug("Insert report: {}", report);
        reportMapper.insert(report);
        writeLog("Insert report#" + report.getId());

        List<Issue> issues = report.getIssues();
        LOGGER.debug("Insert issues {}", issues);

        for (Issue issue : issues) {
            LOGGER.debug("Insert issue {}", issue);
            issueMapper.insert(issue);
            writeLog("Insert issue#" + issue.getId());

            LOGGER.debug("Add issue {} to report {}", issue, report);
            reportMapper.addIssue(report, issue);
            writeLog("Add issue#" + issue.getId() + " to report#" + report.getId());
        }
        return report;
    }

    @Override
    public List<Report> getAll() {
        LOGGER.debug("Get all reports");
        return reportMapper.getAll();
    }

    @Override
    public List<Report> getUserReports(String username) {
        LOGGER.debug("Get all reports for user: {}", username);
        return reportMapper.getUserReports(username);
    }
}
