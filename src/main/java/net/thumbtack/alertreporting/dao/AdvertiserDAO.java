package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.entity.advertiser.Advertiser;

import java.util.List;

public interface AdvertiserDAO {
    List<Advertiser> getAll();
}
