package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.entity.report.Report;

import java.util.List;

public interface ReportDAO {
    Report insert(Report report);

    List<Report> getAll();

    List<Report> getUserReports(String username);
}
