package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.entity.logmessage.LogMessage;

import java.util.List;

public interface LogDAO {
    public List<LogMessage> getAll();
}
