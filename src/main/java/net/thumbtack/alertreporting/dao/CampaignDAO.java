package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.entity.campaign.Campaign;

import java.util.List;

public interface CampaignDAO {

    List<Campaign> getByArchivedStatus(Boolean archived);

    Campaign getById(Integer id);

    List<Campaign> getNotAssigned();

    List<Campaign> getByUserId(Integer userId);

    List<Campaign> getAll();
}
