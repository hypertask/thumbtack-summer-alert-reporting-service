package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.entity.creative.Creative;

import java.util.List;

public interface CreativeDAO {
    List<Creative> getAll();
}
