package net.thumbtack.alertreporting.dao;

import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.user.Role;
import net.thumbtack.alertreporting.entity.user.User;

import java.util.List;

public interface UserDAO {
    User insert(User user);

    Integer update(User user);

    Integer addCampaign(Integer userId, Integer campaignId);

    Integer removeCampaign(Integer userId, Integer campaignId);

    Integer delete(Integer userId);

    User getById(Integer id);

    User getByUsername(String login);

    List<User> getByCampaign(Campaign campaign);

    List<User> getAll();

    List<Role> getAllRoles();
}
