package net.thumbtack.alertreporting.security;

import net.thumbtack.alertreporting.dao.UserDAO;
import net.thumbtack.alertreporting.entity.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
    private UserDAO userDAO;

    @Autowired
    public UserDetailsServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.debug("Get user by username: {}", username);
        User user = userDAO.getByUsername(username);
        UserDetailsImpl userDetailsImpl = new UserDetailsImpl();
        if (user != null) {
            LOGGER.debug("Making UserDetailsImpl from user: {}", user);
            Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
            user.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getRole())));
            userDetailsImpl.setUsername(user.getUsername());
            userDetailsImpl.setPassword(user.getPassword());
            userDetailsImpl.setAuthorities(authorities);
            LOGGER.debug("Made user details: {}", userDetailsImpl);
        }
        return userDetailsImpl;
    }
}
