package net.thumbtack.alertreporting.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtTokenUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenUtil.class);

    private static final String CLAIM_KEY_USERNAME = "sub";
    private static final String CLAIM_KEY_CREATED = "created";
    private static final String CLAIM_KEY_EXPIRED = "exp";
    private static final String CLAIM_KEY_AUTHORITIES = "authorities";

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    public String getUsernameFromToken(String token) {
        LOGGER.debug("Get username from token: {}", token);
        String username;
        try {
            Claims claims = getClaimsFromToken(token);
            username = claims.getSubject();
            LOGGER.debug("Got username: {}", username);
        } catch (Exception e) {
            username = null;
            LOGGER.debug("Failed to get username from token: {}", e.getMessage());
        }
        return username;
    }

    public Date getCreatedDateFromToken(String token) {
        LOGGER.debug("Get created date from token: {}", token);
        Date created;
        try {
            Claims claims = getClaimsFromToken(token);
            created = new Date((Long) claims.get(CLAIM_KEY_CREATED));
            LOGGER.debug("Got created date: {}", created);
        } catch (Exception e) {
            created = null;
            LOGGER.debug("Failed to get created date from token: {}", e.getMessage());
        }
        return created;
    }

    public Date getExpirationDateFromToken(String token) {
        LOGGER.debug("Get expiration date from token: {}", token);
        Date expiration;
        try {
            Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();
            LOGGER.debug("Got expiration date: {}", expiration);
        } catch (Exception e) {
            expiration = null;
            LOGGER.debug("Failed to get expiration date from token: {}", e.getMessage());
        }
        return expiration;
    }

    public Collection<GrantedAuthority> getAuthoritiesFromToken(String token) {
        LOGGER.debug("Get authorities from token: {}", token);
        Collection<GrantedAuthority> authorities;
        try {
            Claims claims = getClaimsFromToken(token);
            authorities = (Collection<GrantedAuthority>) claims.get(CLAIM_KEY_AUTHORITIES);
            LOGGER.debug("Got expiration date: {}", expiration);
        } catch (Exception e) {
            authorities = null;
            LOGGER.debug("Failed to get authorities from token: {}", e.getMessage());
        }
        return authorities;
    }

    private Claims getClaimsFromToken(String token) {
        LOGGER.debug("Get claims from token: {}", token);
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            LOGGER.debug("Got claims: {}", claims.toString());
        } catch (Exception e) {
            claims = null;
            LOGGER.debug("Failed to get claims from token: {}", e.getMessage());
        }
        return claims;
    }

    private Boolean isTokenExpired(String token) {
        LOGGER.debug("Check token expired: {}", token);
        Date expiration = getExpirationDateFromToken(token);
        Boolean expired = expiration.before(new Date());
        LOGGER.debug("Token expired: {}", expired);
        return expired;
    }

    public String generateToken(UserDetails userDetails) {
        LOGGER.debug("Generate token from user details {}", userDetails);
        Map<String, Object> claims = new HashMap<>();

        LOGGER.debug("Put username {} to claims", userDetails.getUsername());
        claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());

        LOGGER.debug("Put authorities {} to claims", userDetails.getAuthorities());
        claims.put(CLAIM_KEY_AUTHORITIES, userDetails.getAuthorities());

        Date createdDate = new Date();
        LOGGER.debug("Put createdDate {} to claims", createdDate);
        claims.put(CLAIM_KEY_CREATED, createdDate);

        return doGenerateToken(claims);
    }

    private String doGenerateToken(Map<String, Object> claims) {
        LOGGER.debug("Generate token from claims {}", claims);
        Date createdDate = (Date) claims.get(CLAIM_KEY_CREATED);
        Date expirationDate = new Date(createdDate.getTime() + expiration * 1000);

        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public Boolean canTokenBeRefreshed(String token) {
        Date created = getCreatedDateFromToken(token);
        return !isTokenExpired(token);
    }

    public String refreshToken(String token) {
        LOGGER.debug("Refresh token: {}", token);
        String refreshedToken;
        try {
            LOGGER.debug("Update created date");
            Claims claims = getClaimsFromToken(token);
            claims.put(CLAIM_KEY_CREATED, new Date());
            refreshedToken = doGenerateToken(claims);
        } catch (Exception e) {
            refreshedToken = null;
            LOGGER.debug("Failed to refresh token: {}", e.getMessage());
        }
        return refreshedToken;
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        LOGGER.debug("Validate token {} using user details {}", token, userDetails);
        UserDetailsImpl user = (UserDetailsImpl) userDetails;
        String username = getUsernameFromToken(token);
        Date created = getCreatedDateFromToken(token);
        Date expiration = getExpirationDateFromToken(token);
        Boolean valid = (username.equals(user.getUsername())
                && !isTokenExpired(token));
        LOGGER.debug("Token is valid: {}", valid);
        return valid;
    }
}

