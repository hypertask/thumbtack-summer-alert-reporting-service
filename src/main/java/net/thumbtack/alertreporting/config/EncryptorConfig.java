package net.thumbtack.alertreporting.config;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EncryptorConfig {
    @Bean
    public StrongPasswordEncryptor encryptor() {
        return new StrongPasswordEncryptor();
    }
}
