package net.thumbtack.alertreporting.service;

import net.thumbtack.alertreporting.dao.LogDAO;
import net.thumbtack.alertreporting.entity.logmessage.LogMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogService.class);

    private LogDAO logDAO;

    @Autowired
    public LogService(LogDAO logDAO) {
        this.logDAO = logDAO;
    }

    public List<LogMessage> getAll() {
        LOGGER.debug("Get all log messages");
        return logDAO.getAll();
    }
}
