package net.thumbtack.alertreporting.service;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CampaignService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignService.class);
    private CampaignDAO campaignDAO;

    @Autowired
    public CampaignService(CampaignDAO campaignDAO) {
        this.campaignDAO = campaignDAO;
    }

    public List<Campaign> getUserCampaigns(Integer userId) {
        LOGGER.debug("Get campaigns by userId: {}", userId);
        return campaignDAO.getByUserId(userId);
    }

    public List<Campaign> getNotAssigned() {
        LOGGER.debug("Get campaigns without manager assigned");
        return campaignDAO.getNotAssigned();
    }
}
