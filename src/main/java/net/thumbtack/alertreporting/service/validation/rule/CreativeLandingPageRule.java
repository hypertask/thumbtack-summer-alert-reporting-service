package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CreativeDAO;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class CreativeLandingPageRule extends AbstractRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreativeLandingPageRule.class);

    @Autowired
    private CreativeDAO creativeDAO;

    public CreativeLandingPageRule() {
        super(RuleInfo.CREATIVE_LANDING_PAGE_SUBDOMAIN_OF_ADVERTISER_SITE);
    }

    @Override
    public List<Report> validate() {
        LOGGER.debug("Validating creatives using rule: {}", getRuleInfo());
        List<Report> reports = new ArrayList<>();
        List<Creative> creatives = creativeDAO.getAll();
        LOGGER.debug("Creatives to validate: {}", creatives);

        creatives
                .stream()
                .filter(creative -> creative.getAdvertiser() != null)
                .filter(creative -> creative.getLandingPageUrl() != null && creative.getAdvertisersSite() != null)
                .filter(creative -> !creative.getLandingPageUrl().contains(creative.getAdvertisersSite()))
                .forEach(creative -> reports.add(new Report(new ArrayList<>(Arrays.asList(new Issue(
                        "Creative#%d landing page doesn't match its advertiser site domain", creative.getId()))),
                        new Date())));

        return reports;
    }
}
