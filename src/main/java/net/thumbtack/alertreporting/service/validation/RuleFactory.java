package net.thumbtack.alertreporting.service.validation;

import net.thumbtack.alertreporting.service.validation.rule.AbstractRule;

public abstract class RuleFactory {
    public abstract AbstractRule getRule(String ruleName);
}
