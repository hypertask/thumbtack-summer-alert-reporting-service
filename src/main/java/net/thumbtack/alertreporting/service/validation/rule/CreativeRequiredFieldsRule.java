package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CreativeDAO;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.creative.CreativeStatus;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CreativeRequiredFieldsRule extends AbstractRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreativeRequiredFieldsRule.class);

    @Autowired
    private CreativeDAO creativeDAO;

    public CreativeRequiredFieldsRule() {
        super(RuleInfo.CREATIVE_REQUIRED_FIELDS_FILLED);
    }

    @Override
    public List<Report> validate() {
        LOGGER.debug("Validating creatives using rule: {}", getRuleInfo());
        List<Report> reports = new ArrayList<>();
        List<Creative> creatives = creativeDAO.getAll();
        LOGGER.debug("Cretives to validate: {}", creatives);

        for (Creative creative : creatives) {
            List<Issue> issues = new ArrayList<>();

            if (CreativeStatus.INACTIVE.equals(creative.getStatus())) {
                continue;
            }
            if (creative.getName() == null || creative.getName().isEmpty()) {
                issues.add(new Issue("Creative#%d name is empty", creative.getId()));
            }
            if (creative.getStatus() == null) {
                issues.add(new Issue("Creative#%d status is empty", creative.getId()));
            }
            if (creative.getType() == null) {
                issues.add(new Issue("Creative#%d type is empty", creative.getId()));
            }
            if (creative.getDimensions() == null) {
                issues.add(new Issue("Creative#%d dimensions is empty", creative.getId()));
            }

            if (issues.size() == 0) {
                LOGGER.debug("No issues found for creative: {}", creative);
                continue;
            }

            LOGGER.debug("Found issues: {} for creative: {}", issues, creative);
            if (creative.getCampaign() != null) {
                reports.add(new Report(creative.getCampaignId(), creative.getCampaignName(), issues, new Date()));
            } else {
                reports.add(new Report(issues, new Date()));
            }
        }
        return reports;
    }
}
