package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class CampaignCreativesCategoriesMatchRule extends AbstractRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignCreativesCategoriesMatchRule.class);

    @Autowired
    private CampaignDAO campaignDAO;

    public CampaignCreativesCategoriesMatchRule() {
        super(RuleInfo.CAMPAIGN_CREATIVES_CATEGORIES_MATCH);
    }

    @Override
    public List<Report> validate() {
        LOGGER.debug("Validating campaigns using rule: {}", getRuleInfo());
        List<Report> reports = new ArrayList<>();
        List<Campaign> campaigns = campaignDAO.getAll();
        LOGGER.debug("Campaigns to validate: {}", campaigns);

        campaigns
                .stream()
                .filter(campaign -> (campaign.getCategories() == null || campaign.getCategories().isEmpty()) &&
                        campaign.getCreatives()
                                .stream()
                                .map(Creative::getCategories)
                                .anyMatch(list -> !list.isEmpty()))
                .forEach(campaign -> reports.add(
                        new Report(campaign.getId(), campaign.getName(),
                                new ArrayList<>(Arrays.asList(new Issue(
                                        "Creatives have categories while parent campaign doesn't"))), new Date())));

        campaigns
                .stream()
                .filter(campaign -> campaign.getCreatives() != null)
                .filter(campaign -> campaign.getCategories() != null && !campaign.getCategories().isEmpty())
                .forEach(campaign -> {
                    campaign.getCreatives()
                            .stream()
                            .filter(creative -> creative.getCategories() != null)
                            .forEach(creative -> {
                                creative.getCategories()
                                        .stream()
                                        .filter(creativeCategory -> campaign.getCategories()
                                                .stream()
                                                .noneMatch(creativeCategory::isSubcategoryOf))
                                        .forEach(category -> reports.add(new Report(
                                                campaign.getId(), campaign.getName(),
                                                new ArrayList<>(Arrays.asList(new Issue(
                                                        "Creative#%d categories mismatch", creative.getId()))),
                                                new Date())));
                            });
                });
        return reports;
    }
}
