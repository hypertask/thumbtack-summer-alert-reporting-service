package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.AdvertiserDAO;
import net.thumbtack.alertreporting.entity.advertiser.Advertiser;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class AdvertiserRequiredFieldsRule extends AbstractRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdvertiserRequiredFieldsRule.class);

    @Autowired
    private AdvertiserDAO advertiserDAO;

    public AdvertiserRequiredFieldsRule() {
        super(RuleInfo.ADVERTISER_REQUIRED_FIELDS_FILLED);
    }

    @Override
    public List<Report> validate() {
        LOGGER.debug("Validating advertiser using rule: {}", getRuleInfo());
        List<Report> reports = new ArrayList<>();
        List<Advertiser> advertisers = advertiserDAO.getAll();
        LOGGER.debug("Advertisers to validate: {}", advertisers);

        for (Advertiser advertiser : advertisers) {
            LOGGER.debug("Validating advertiser: {}", advertiser);
            List<Issue> issues = new ArrayList<>();
            if (advertiser.getName() == null || advertiser.getName().isEmpty()) {
                issues.add(new Issue("Advertiser#%d name is empty", advertiser.getId()));
            }

            if (issues.size() == 0) {
                LOGGER.debug("No issues found for advertiser: {}", advertiser);
                continue;
            }

            LOGGER.debug("Found issues: {} for advertiser: {}", issues, advertiser);
            reports.add(new Report(issues, new Date()));
        }
        return reports;
    }
}
