package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.campaign.CampaignStatus;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CampaignStartTimeNotInPastRule extends AbstractRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignStartTimeNotInPastRule.class);

    @Autowired
    private CampaignDAO campaignDAO;

    public CampaignStartTimeNotInPastRule() {
        super(RuleInfo.CAMPAIGN_START_TIME_NOT_IN_PAST);
    }


    @Override
    public List<Report> validate() {
        LOGGER.debug("Validating campaigns using rule: {}", getRuleInfo());
        List<Report> reports = new ArrayList<>();
        List<Campaign> campaigns = campaignDAO.getAll();
        LOGGER.debug("Campaigns to validate: {}", campaigns);

        for (Campaign campaign : campaigns) {
            LOGGER.debug("Validating campaign: {}", campaign);
            List<Issue> issues = new ArrayList<>();

            if (CampaignStatus.DRAFT.equals(campaign.getStatus())) {
                if (campaign.getStartDate().before(new Date())) {
                    issues.add(new Issue("Campaign with draft status has start time in past"));
                }
            }

            if (issues.size() == 0) {
                LOGGER.debug("No issues found for campaign: {}", campaign);
                continue;
            }

            LOGGER.debug("Found issues: {} for campaign: {}", issues, campaign);
            reports.add(new Report(campaign.getId(), campaign.getName(), issues, new Date()));
        }
        return reports;
    }
}
