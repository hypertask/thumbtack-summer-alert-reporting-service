package net.thumbtack.alertreporting.service.validation.rule;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum RuleInfo {
    ADVERTISER_REQUIRED_FIELDS_FILLED("Advertisers required fields must be filled"),
    CAMPAIGN_REQUIRED_FIELDS_FILLED("Campaigns required fields must be filled"),
    CAMPAIGN_HAS_ACTIVE_CREATIVES("Active campaigns should have at least one active creative"),
    CREATIVE_REQUIRED_FIELDS_FILLED("Creatives required fields must be filled"),
    CAMPAIGN_START_TIME_NOT_IN_PAST("Campaigns with draft status should'nt have start time in past"),
    CAMPAIGN_PRICE_NOT_LOWER_THAN_CREATIVES_PRICE("Campaigns price should't be lower than its creatives price"),
    CAMPAIGN_ACTIVE_CREATIVES_HAVE_SAME_PRICE("Campaigns active creatives should have same price"),
    ARCHIVED_CAMPAIGN_STATUS_ENDED_OR_PAUSED("Archived campaign should have status ended or paused"),
    CREATIVE_LANDING_PAGE_SUBDOMAIN_OF_ADVERTISER_SITE("Creatives landing page should be same as or subdomain advertisers site"),
    CAMPAIGN_CREATIVES_CATEGORIES_MATCH("Creatives categories should match it's campaign categories"),
    CAMPAIGN_HAS_MANAGER_ASSIGNED("Unfinished campaigns should have manager assigned");

    private String code;
    private String description;

    RuleInfo(String description) {
        this.code = name();
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "RuleInfo{" +
                "description='" + description + '\'' +
                '}';
    }
}
