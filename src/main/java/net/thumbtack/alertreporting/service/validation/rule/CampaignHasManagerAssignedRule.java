package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.dao.UserDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.campaign.CampaignStatus;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import net.thumbtack.alertreporting.entity.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class CampaignHasManagerAssignedRule extends AbstractRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignHasManagerAssignedRule.class);

    @Autowired
    private CampaignDAO campaignDAO;
    @Autowired
    private UserDAO userDAO;

    public CampaignHasManagerAssignedRule() {
        super(RuleInfo.CAMPAIGN_HAS_MANAGER_ASSIGNED);
    }

    @Override
    public List<Report> validate() {
        LOGGER.debug("Validating campaigns using rule: {}", getRuleInfo());
        List<Report> reports = new ArrayList<>();
        List<Campaign> campaigns = campaignDAO.getAll();
        LOGGER.debug("Campaigns to validate: {}", campaigns);

        campaigns
                .stream()
                .filter(campaign -> !CampaignStatus.ENDED.equals(campaign.getStatus()))
                .forEach(campaign -> {
                    List<User> users = userDAO.getByCampaign(campaign);

                    if (users == null || users.isEmpty()) {
                        reports.add(new Report(campaign.getId(), campaign.getName(),
                                new ArrayList<>(Arrays.asList(new Issue("Unfinished campaign has no manager assigned"))), new Date()));
                    }

                    if (users != null && users.size() > 1) {
                        reports.add(new Report(campaign.getId(), campaign.getName(),
                                new ArrayList<>(Arrays.asList(new Issue("Campaign has more than one manager assigned"))), new Date()));
                    }
                });

        return reports;
    }
}
