package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.campaign.CampaignStatus;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class ArchivedCampaignStatusEndedOrPausedRule extends AbstractRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArchivedCampaignStatusEndedOrPausedRule.class);
    @Autowired
    private CampaignDAO campaignDAO;

    public ArchivedCampaignStatusEndedOrPausedRule() {
        super(RuleInfo.ARCHIVED_CAMPAIGN_STATUS_ENDED_OR_PAUSED);
    }

    @Override
    public List<Report> validate() {
        LOGGER.debug("Validating campaigns using rule: {}", getRuleInfo());
        List<Report> reports = new ArrayList<>();
        List<Campaign> campaigns = campaignDAO.getByArchivedStatus(true);
        LOGGER.debug("Campaigns to validate: {}", campaigns);

        campaigns
                .stream()
                .filter(campaign -> !CampaignStatus.PAUSED.equals(campaign.getStatus()) &&
                        !CampaignStatus.ENDED.equals(campaign.getStatus()))
                .forEach(campaign ->
                        reports.add(new Report(campaign.getId(), campaign.getName(),
                                new ArrayList<>(Arrays.asList(new Issue("Archived campaign has active or draft status"))), new Date())
                        ));
        return reports;
    }
}
