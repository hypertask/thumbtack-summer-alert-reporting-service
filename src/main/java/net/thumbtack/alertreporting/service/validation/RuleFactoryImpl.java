package net.thumbtack.alertreporting.service.validation;

import net.thumbtack.alertreporting.service.validation.rule.AbstractRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public final class RuleFactoryImpl extends RuleFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(RuleFactoryImpl.class);
    private static final Map<String, AbstractRule> RULES = new HashMap<>();

    @Autowired
    private List<AbstractRule> rulesList;

    @PostConstruct
    private void initRules() {
        LOGGER.info("Initializing validation rules: {}", rulesList);
        for (AbstractRule rule : rulesList) {
            RULES.put(rule.getRuleInfo(), rule);
        }
    }

    public AbstractRule getRule(String ruleName) {
        LOGGER.debug("Getting rule by name: {}", ruleName);
        AbstractRule rule = RULES.get(ruleName);
        if (rule == null) {
            LOGGER.error("No such rule: {}", ruleName);
            throw new IllegalArgumentException("No such rule: " + ruleName);
        }
        return rule;
    }
}
