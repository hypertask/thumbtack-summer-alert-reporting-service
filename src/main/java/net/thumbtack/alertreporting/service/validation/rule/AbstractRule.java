package net.thumbtack.alertreporting.service.validation.rule;


import net.thumbtack.alertreporting.entity.report.Report;

import java.util.List;

public abstract class AbstractRule {
    private final RuleInfo ruleInfo;

    public AbstractRule(RuleInfo ruleInfo) {
        this.ruleInfo = ruleInfo;
    }

    public abstract List<Report> validate();

    public String getRuleInfo() {
        return ruleInfo.name();
    }

    @Override
    public String toString() {
        return "AbstractRule{" +
                "ruleInfo=" + ruleInfo +
                '}';
    }
}
