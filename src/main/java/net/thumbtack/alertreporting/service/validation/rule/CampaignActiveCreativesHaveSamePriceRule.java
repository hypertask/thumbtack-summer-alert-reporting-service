package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.creative.CreativeStatus;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CampaignActiveCreativesHaveSamePriceRule extends AbstractRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignActiveCreativesHaveSamePriceRule.class);
    @Autowired
    private CampaignDAO campaignDAO;

    public CampaignActiveCreativesHaveSamePriceRule() {
        super(RuleInfo.CAMPAIGN_ACTIVE_CREATIVES_HAVE_SAME_PRICE);
    }

    @Override
    public List<Report> validate() {
        LOGGER.debug("Validating campaigns using rule: {}", getRuleInfo());
        List<Report> reports = new ArrayList<>();
        List<Campaign> campaigns = campaignDAO.getAll();
        LOGGER.debug("Campaigns to validate: {}", campaigns);

        campaigns
                .stream()
                .filter(campaign -> campaign.getCreatives() != null)
                .filter(campaign -> campaign.getCreatives()
                        .stream()
                        .filter(creative -> CreativeStatus.ACTIVE.equals(creative.getStatus()))
                        .filter(creative -> creative.getPrice() != null)
                        .collect(Collectors.groupingBy(Creative::getPrice))
                        .values()
                        .size() > 1)
                .forEach(campaign -> reports.add(new Report(campaign.getId(), campaign.getName(),
                        new ArrayList<>(Arrays.asList(new Issue("Campaign has active creatives with different price"))), new Date())));

        return reports;
    }
}
