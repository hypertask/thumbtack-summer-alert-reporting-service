package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.campaign.CampaignStatus;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.creative.CreativeStatus;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Component
public class CampaignRequiredFieldsRule extends AbstractRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignRequiredFieldsRule.class);

    @Autowired
    private CampaignDAO campaignDAO;

    public CampaignRequiredFieldsRule() {
        super(RuleInfo.CAMPAIGN_REQUIRED_FIELDS_FILLED);
    }

    @Override
    public List<Report> validate() {
        LOGGER.debug("Validating campaigns using rule: {}", getRuleInfo());
        List<Report> reports = new ArrayList<>();
        List<Campaign> campaigns = campaignDAO.getAll();
        LOGGER.debug("Campaigns to validate: {}", campaigns);

        for (Campaign campaign : campaigns) {
            LOGGER.debug("Validating campaign: {}", campaign);
            List<Issue> issues = new ArrayList<>();

            if (CampaignStatus.DRAFT.equals(campaign.getStatus())) {
                continue;
            }
            if (campaign.getName() == null || campaign.getName().isEmpty()) {
                issues.add(new Issue("Campaign name is empty"));
            }
            if (campaign.getStatus() == null) {
                issues.add(new Issue("Campaign status is empty"));
            }
            if (campaign.getStartDate() == null) {
                issues.add(new Issue("Campaign start date is empty"));
            }
            if (campaign.getEndDate() == null) {
                issues.add(new Issue("Campaign end date is empty"));
            }
            if (campaign.getImpressionCap() == null) {
                issues.add(new Issue("Campaign impression cap is empty"));
            }
            if (campaign.getBudget() == null) {
                issues.add(new Issue("Campaign budget is empty"));
            }
            if (campaign.getPrice() == null && getMaxPriceFromActiveCreatives(campaign) == null) {
                issues.add(new Issue("Campaign has no price"));
            }

            if (issues.size() == 0) {
                LOGGER.debug("No issues found for campaign: {}", campaign);
                continue;
            }

            LOGGER.debug("Found issues: {} for campaign: {}", issues, campaign);
            reports.add(new Report(campaign.getId(), campaign.getName(), issues, new Date()));
        }
        return reports;
    }

    private BigDecimal getMaxPriceFromActiveCreatives(Campaign campaign) {
        List<Creative> creatives = campaign.getCreatives();

        if (creatives == null) {
            return null;
        }

        return creatives
                .stream()
                .filter(creative -> CreativeStatus.ACTIVE.equals(creative.getStatus()))
                .max(Comparator.comparing(Creative::getPrice))
                .map(Creative::getPrice)
                .orElse(null);
    }
}
