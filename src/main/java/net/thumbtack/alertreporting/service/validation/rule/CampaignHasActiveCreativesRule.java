package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.campaign.CampaignStatus;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.creative.CreativeStatus;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CampaignHasActiveCreativesRule extends AbstractRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignHasActiveCreativesRule.class);
    @Autowired
    private CampaignDAO campaignDAO;

    public CampaignHasActiveCreativesRule() {
        super(RuleInfo.CAMPAIGN_HAS_ACTIVE_CREATIVES);
    }

    @Override
    public List<Report> validate() {
        LOGGER.debug("Validating campaigns using rule: {}", getRuleInfo());
        List<Report> reports = new ArrayList<>();
        List<Campaign> campaigns = campaignDAO.getAll();
        LOGGER.debug("Campaigns to validate: {}", campaigns);

        for (Campaign campaign : campaigns) {
            LOGGER.debug("Validating campaign: {}", campaign);
            List<Issue> issues = new ArrayList<>();
            if (CampaignStatus.ACTIVE.equals(campaign.getStatus())) {
                List<Creative> creatives = campaign.getCreatives();

                if (creatives != null) {
                    boolean hasActiveCreatives = creatives
                            .stream()
                            .anyMatch(p -> CreativeStatus.ACTIVE.equals(p.getStatus()));

                    if (!hasActiveCreatives) {
                        issues.add(new Issue("Active campaign has no active creatives"));
                    }
                } else {
                    issues.add(new Issue("Active campaign has no creatives"));
                }

                if (issues.size() == 0) {
                    LOGGER.debug("No issues found for campaign: {}", campaign);
                    continue;
                }

                LOGGER.debug("Found issues: {} for campaign: {}", issues, campaign);
                reports.add(new Report(campaign.getId(), campaign.getName(), issues, new Date()));
            }
        }
        return reports;
    }
}
