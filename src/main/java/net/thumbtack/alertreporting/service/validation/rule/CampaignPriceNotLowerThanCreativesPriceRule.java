package net.thumbtack.alertreporting.service.validation.rule;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class CampaignPriceNotLowerThanCreativesPriceRule extends AbstractRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignPriceNotLowerThanCreativesPriceRule.class);
    @Autowired
    private CampaignDAO campaignDAO;

    public CampaignPriceNotLowerThanCreativesPriceRule() {
        super(RuleInfo.CAMPAIGN_PRICE_NOT_LOWER_THAN_CREATIVES_PRICE);
    }

    @Override
    public List<Report> validate() {
        LOGGER.debug("Validating campaigns using rule: {}", getRuleInfo());
        List<Report> reports = new ArrayList<>();
        List<Campaign> campaigns = campaignDAO.getAll();
        LOGGER.debug("Campaigns to validate: {}", campaigns);

        campaigns
                .stream()
                .filter(campaign -> campaign.getPrice() != null && campaign.getCreatives() != null)
                .forEach(campaign -> campaign.getCreatives()
                        .stream()
                        .filter(creative -> creative.getPrice() != null)
                        .filter(creative -> creative.getPrice().compareTo(campaign.getPrice()) == 1)
                        .forEach(creative -> reports.add(new Report(campaign.getId(), campaign.getName(),
                                new ArrayList<>(Arrays.asList(
                                        new Issue("Campaign has price lower than its creative#%d price",
                                                creative.getId()))), new Date()))));

        return reports;
    }
}
