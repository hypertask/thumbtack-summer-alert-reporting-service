package net.thumbtack.alertreporting.service;

import net.thumbtack.alertreporting.dao.ReportDAO;
import net.thumbtack.alertreporting.entity.report.Report;
import net.thumbtack.alertreporting.service.validation.RuleFactory;
import net.thumbtack.alertreporting.service.validation.rule.RuleInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toMap;

@Service
public class ReportService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);
    private ReportDAO reportDAO;
    private RuleFactory ruleFactory;

    @Autowired
    public ReportService(ReportDAO reportDAO) {
        this.reportDAO = reportDAO;
    }

    @Autowired
    public void setRuleFactory(RuleFactory ruleFactory) {
        this.ruleFactory = ruleFactory;
    }

    @Transactional
    public List<Report> validate(List<String> ruleNames) {
        LOGGER.debug("Validate using rules: {}", ruleNames);
        List<Report> reports = new ArrayList<>();

        for (String ruleName : ruleNames) {
            LOGGER.debug("Validation using rule: {}", ruleName);
            reports.addAll(ruleFactory.getRule(ruleName).validate());
        }

        Collection<Report> reportMap = reports.stream()
                .collect(toMap(Report::getCampaignId, report -> report, Report::merge))
                .values();

        LOGGER.debug("Inserting reports to database");
        for (Report report : reportMap) {
            LOGGER.debug("Inserting report: {}", report);
            reportDAO.insert(report);
        }

        return new ArrayList<>(reportMap);
    }

    public List<Report> getAll() {
        LOGGER.debug("Get all reports");
        return reportDAO.getAll();
    }

    public List<Report> getUserReports() {
        if(SecurityContextHolder.getContext().getAuthentication() != null) {
            String username = SecurityContextHolder
                    .getContext().getAuthentication().getName();
            LOGGER.debug("Get all reports for user: {}", username);
            return reportDAO.getUserReports(username);
        }
        return reportDAO.getAll();
    }

    public RuleInfo[] getRules() {
        LOGGER.debug("Get rule codes");
        return RuleInfo.values();
    }
}
