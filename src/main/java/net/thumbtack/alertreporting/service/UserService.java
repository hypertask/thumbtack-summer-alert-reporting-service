package net.thumbtack.alertreporting.service;

import net.thumbtack.alertreporting.dao.CampaignDAO;
import net.thumbtack.alertreporting.dao.UserDAO;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.user.Role;
import net.thumbtack.alertreporting.entity.user.User;
import net.thumbtack.alertreporting.security.EncryptionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);
    private EncryptionService encryptionService;
    private UserDAO userDAO;
    private CampaignDAO campaignDAO;

    @Autowired
    public UserService(UserDAO userDAO, CampaignDAO campaignDAO) {
        this.userDAO = userDAO;
        this.campaignDAO = campaignDAO;
    }

    @Autowired
    public void setEncryptionService(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @Transactional
    public User addUser(User user) {
        LOGGER.debug("Add user: {}", user);
        LOGGER.debug("Check if user with such name already exists");
        User registered = userDAO.getByUsername(user.getUsername());
        if (registered != null) {
            LOGGER.warn("User with such name already exist: ", user.getUsername());
            throw new IllegalArgumentException("User with such name already exist: " + user.getUsername());
        }
        String encryptedPassword = encryptionService.encryptPassword(user.getPassword());
        user.setPassword(encryptedPassword);
        return userDAO.insert(user);
    }

    @Transactional
    public void update(User user) {
        LOGGER.debug("Update user: {}", user);

        User userExists = userDAO.getById(user.getId());
        if (userExists == null) {
            LOGGER.warn("User doesn't exist: {}", user);
            throw new IllegalArgumentException("User doesn't exist: " + user);
        }

        String encryptedPassword = encryptionService.encryptPassword(user.getPassword());
        user.setPassword(encryptedPassword);

        int updated = userDAO.update(user);
        LOGGER.debug("Updated {} rows", updated);
    }

    @Transactional
    public void assign(Integer userId, Integer campaignId) {
        LOGGER.debug("Assign user with id {} to campaign with id {}", userId, campaignId);

        User user = userDAO.getById(userId);
        if (user == null) {
            LOGGER.warn("User with id {} doesn't exist", userId);
            throw new IllegalArgumentException("User with id " + userId + " doesn't exist");
        }
        Campaign campaign = campaignDAO.getById(campaignId);
        if (campaign == null) {
            LOGGER.warn("Campaign with id {} doesn't exist", campaignId);
            throw new IllegalArgumentException("Campaign with id " + campaignId + " doesn't exist");
        }
        List<User> users = userDAO.getByCampaign(campaign);
        if (users != null && !users.isEmpty()) {
            LOGGER.warn("Campaign already have assigned manager");
            throw new IllegalArgumentException("Campaign already have assigned manager");
        }

        int updated = userDAO.addCampaign(userId, campaignId);
        LOGGER.debug("Updated {} rows", updated);
    }

    @Transactional
    public void unassign(Integer userId, Integer campaignId) {
        LOGGER.debug("Unassign user with id {} from campaign with id {}", userId, campaignId);

        User user = userDAO.getById(userId);
        if (user == null) {
            LOGGER.warn("User with id {} doesn't exist", userId);
            throw new IllegalArgumentException("User with id " + userId + " doesn't exist");
        }
        Campaign campaign = campaignDAO.getById(campaignId);
        if (campaign == null) {
            LOGGER.warn("Campaign with id {} doesn't exist", campaignId);
            throw new IllegalArgumentException("Campaign with id " + campaignId + " doesn't exist");
        }
        List<User> users = userDAO.getByCampaign(campaign);
        if (!users.contains(user)) {
            LOGGER.warn("User {} wasn't assigned to campaign {}", user, campaign);
            throw new IllegalArgumentException("User wasn't assigned to given campaign");
        }

        int updated = userDAO.removeCampaign(userId, campaignId);
        LOGGER.debug("Updated {} rows", updated);
    }

    @Transactional
    public void delete(Integer userId) {
        LOGGER.debug("Delete user with id: {}", userId);

        int updated = userDAO.delete(userId);
        LOGGER.debug("Updated {} rows", updated);
    }

    public List<User> getAll() {
        LOGGER.debug("Get all users");
        return userDAO.getAll();
    }

    public User get(Integer userId) {
        LOGGER.debug("Get user with id: {}", userId);
        return userDAO.getById(userId);
    }

    public List<Role> getAllRoles() {
        LOGGER.debug("Get all roles");
        return userDAO.getAllRoles();
    }
}
