package net.thumbtack.alertreporting.controller;

import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.service.CampaignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/api")
public class CampaignController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignController.class);
    private CampaignService campaignService;

    @Autowired
    public CampaignController(CampaignService campaignService) {
        this.campaignService = campaignService;
    }

    @RequestMapping(value = "/users/{userId}/campaigns", method = RequestMethod.GET)
    @ResponseBody
    public List<Campaign> getUserCampaigns(@PathVariable("userId") Integer userId) {
        LOGGER.debug("Get campaigns by userId: {}", userId);
        return campaignService.getUserCampaigns(userId);
    }

    @RequestMapping(value = "/campaigns/free", method = RequestMethod.GET)
    @ResponseBody
    public List<Campaign> getCampaignsWithoutManage() {
        LOGGER.debug("Get campaign without manager assigned");
        return campaignService.getNotAssigned();
    }
}
