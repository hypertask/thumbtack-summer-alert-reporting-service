package net.thumbtack.alertreporting.controller;

import net.thumbtack.alertreporting.entity.logmessage.LogMessage;
import net.thumbtack.alertreporting.service.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/api")
public class LogController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogController.class);

    private LogService logService;

    @Autowired
    public LogController(LogService logService) {
        this.logService = logService;
    }

    @RequestMapping(value = "/logs", method = RequestMethod.GET)
    @ResponseBody
    public List<LogMessage> getAll() {
        LOGGER.debug("Get all logs");
        return logService.getAll();
    }
}
