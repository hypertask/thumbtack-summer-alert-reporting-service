package net.thumbtack.alertreporting.controller;

import net.thumbtack.alertreporting.entity.user.Role;
import net.thumbtack.alertreporting.entity.user.User;
import net.thumbtack.alertreporting.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/api")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    @ResponseBody
    public User add(@Valid @RequestBody User user) {
        LOGGER.debug("Add user: {}", user);
        return userService.addUser(user);
    }

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.PUT)
    @ResponseBody
    public void update(@PathVariable("userId") Integer userId, @Valid @RequestBody User user) {
        user.setId(userId);
        LOGGER.debug("Update user: {}", user);
        userService.update(user);
    }

    @RequestMapping(value = "/users/{userId}/campaigns/{campaignId}", method = RequestMethod.PUT)
    @ResponseBody
    public void assign(@PathVariable("userId") Integer userId, @PathVariable("campaignId") Integer campaignId) {
        LOGGER.debug("Assign user with id {} to campaign with id {}", userId, campaignId);
        userService.assign(userId, campaignId);
    }

    @RequestMapping(value = "/users/{userId}/campaigns/{campaignId}", method = RequestMethod.DELETE)
    @ResponseBody
    public void unassign(@PathVariable("userId") Integer userId, @PathVariable("campaignId") Integer campaignId) {
        LOGGER.debug("Unassign user with id {} from campaign with id {}", userId, campaignId);
        userService.unassign(userId, campaignId);
    }

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("userId") Integer userId) {
        LOGGER.debug("Delete user with id: {}", userId);
        userService.delete(userId);
    }

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public User get(@PathVariable("userId") Integer userId) {
        LOGGER.debug("Get user with id: {}", userId);
        return userService.get(userId);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getAll() {
        LOGGER.debug("Get all users");
        return userService.getAll();
    }

    @RequestMapping(value = "/users/roles", method = RequestMethod.GET)
    @ResponseBody
    public List<Role> getAllRoles() {
        LOGGER.debug("Get all roles");
        return userService.getAllRoles();
    }
}
