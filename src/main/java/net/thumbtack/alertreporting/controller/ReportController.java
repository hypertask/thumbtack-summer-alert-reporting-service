package net.thumbtack.alertreporting.controller;

import net.thumbtack.alertreporting.entity.report.Report;
import net.thumbtack.alertreporting.service.ReportService;
import net.thumbtack.alertreporting.service.validation.rule.RuleInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api")
public class ReportController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);
    private ReportService reportService;

    @Autowired
    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @RequestMapping(value = "/reports", method = RequestMethod.POST)
    @ResponseBody
    public List<Report> validate(@RequestBody List<String> rules) {
        LOGGER.debug("Validate using rules {}", rules);
        return reportService.validate(rules);
    }

    @RequestMapping(value = "/reports", method = RequestMethod.GET)
    @ResponseBody
    public List<Report> getUserReports() {
        LOGGER.debug("Get all reports for user");
        return reportService.getUserReports();
    }

    @RequestMapping(value = "/reports/rules", method = RequestMethod.GET)
    @ResponseBody
    public RuleInfo[] getRules() {
        LOGGER.debug("Get rule codes");
        return reportService.getRules();
    }
}
