package net.thumbtack.alertreporting.mapper;

import net.thumbtack.alertreporting.entity.report.Issue;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface IssueMapper {
    @Insert("INSERT INTO issues VALUES (DEFAULT, #{issue})")
    @Options(useGeneratedKeys = true)
    Integer insert(Issue issues);

    @Select("SELECT * FROM issues WHERE id IN (SELECT issueId FROM reports_issues WHERE reportId = #{id})")
    List<Issue> getByReport();
}
