package net.thumbtack.alertreporting.mapper;

import net.thumbtack.alertreporting.entity.report.Issue;
import net.thumbtack.alertreporting.entity.report.Report;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

@Mapper
public interface ReportMapper {

    @Insert("INSERT INTO reports VALUES (DEFAULT, #{campaignId}, #{campaignName}, #{created})")
    @Options(useGeneratedKeys = true)
    Integer insert(Report report);

    @Insert("INSERT INTO reports_issues VALUES(DEFAULT, #{report.id}, #{issue.id})")
    Integer addIssue(@Param("report") Report report, @Param("issue") Issue issue);

    @Select("SELECT * FROM reports ORDER BY created DESC")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "issues", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.alertreporting.mapper.IssueMapper.getByReport",
                            fetchType = FetchType.EAGER))
    })
    List<Report> getAll();

    @Select("SELECT * FROM reports WHERE campaignId IN " +
            "(SELECT campaignId FROM users_campaigns WHERE userId IN " +
            "(SELECT id FROM users WHERE username = #{username})) " +
            "OR NOT EXISTS (SELECT campaignid FROM users_campaigns WHERE reports.campaignid = users_campaigns.campaignid) " +
            "ORDER BY created DESC")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "issues", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.alertreporting.mapper.IssueMapper.getByReport",
                            fetchType = FetchType.EAGER))
    })
    List<Report> getUserReports(String username);
}
