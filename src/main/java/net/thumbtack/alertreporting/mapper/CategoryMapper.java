package net.thumbtack.alertreporting.mapper;

import net.thumbtack.alertreporting.entity.category.Category;
import net.thumbtack.alertreporting.entity.creative.Creative;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryMapper {
    @Select("SELECT * FROM categories WHERE id IN (SELECT categoryId FROM creatives_categories WHERE creativeId = #{id})")
    List<Category> getByCreative(Creative creative);

    @Select("SELECT * FROM categories WHERE id IN (SELECT campaignId FROM campaigns_categories WHERE campaignId = #{id})")
    List<Category> getByCampaign(Creative creative);
}
