package net.thumbtack.alertreporting.mapper;

import net.thumbtack.alertreporting.entity.advertiser.Advertiser;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.creative.Creative;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

@Mapper
public interface AdvertiserMapper {
    @Select("SELECT * FROM advertisers WHERE id IN (SELECT advertiserId FROM campaigns WHERE id = #{id})")
    Advertiser getByCampaign(Campaign campaign);

    @Select("SELECT * FROM advertisers WHERE id IN (SELECT advertiserId FROM creatives WHERE id = #{id})")
    Advertiser getByCreative(Creative creative);

    @Select("SELECT * FROM advertisers")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "campaigns", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.alertreporting.mapper.CampaignMapper.getByAdvertiser",
                            fetchType = FetchType.EAGER))})
    List<Advertiser> getAll();
}
