package net.thumbtack.alertreporting.mapper;

import net.thumbtack.alertreporting.entity.advertiser.Advertiser;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.category.Category;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.creative.CreativeDimension;
import net.thumbtack.alertreporting.entity.creative.CreativeStatus;
import net.thumbtack.alertreporting.entity.creative.CreativeType;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

@Mapper
public interface CreativeMapper {
    @Select("SELECT * FROM creatives")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "campaign", column = "id", javaType = Campaign.class,
                    one = @One(select = "net.thumbtack.alertreporting.mapper.CampaignMapper.getByCreative",
                            fetchType = FetchType.EAGER)),
            @Result(property = "advertiser", column = "id", javaType = Advertiser.class,
                    one = @One(select = "net.thumbtack.alertreporting.mapper.AdvertiserMapper.getByCreative",
                            fetchType = FetchType.EAGER)),
            @Result(property = "status", column = "id", javaType = CreativeStatus.class,
                    one = @One(select = "net.thumbtack.alertreporting.mapper.CreativeMapper.getStatus",
                            fetchType = FetchType.EAGER)),
            @Result(property = "type", column = "id", javaType = CreativeType.class,
                    one = @One(select = "net.thumbtack.alertreporting.mapper.CreativeMapper.getType",
                            fetchType = FetchType.EAGER)),
            @Result(property = "dimensions", column = "id", javaType = CreativeDimension.class,
                    one = @One(select = "net.thumbtack.alertreporting.mapper.CreativeMapper.getDimensions",
                            fetchType = FetchType.EAGER)),
            @Result(property = "categories", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.alertreporting.mapper.CategoryMapper.getByCreative",
                            fetchType = FetchType.EAGER))})
    List<Creative> getAll();

    @Select("SELECT * FROM creatives WHERE campaignId = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "status", column = "id", javaType = CreativeStatus.class,
                    one = @One(select = "net.thumbtack.alertreporting.mapper.CreativeMapper.getStatus",
                            fetchType = FetchType.EAGER)),
            @Result(property = "type", column = "id", javaType = CreativeType.class,
                    one = @One(select = "net.thumbtack.alertreporting.mapper.CreativeMapper.getType",
                            fetchType = FetchType.EAGER)),
            @Result(property = "dimensions", column = "id", javaType = CreativeDimension.class,
                    one = @One(select = "net.thumbtack.alertreporting.mapper.CreativeMapper.getDimensions",
                            fetchType = FetchType.EAGER)),
            @Result(property = "categories", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.alertreporting.mapper.CategoryMapper.getByCreative",
                            fetchType = FetchType.EAGER))})
    List<Creative> getByCampaign(Campaign campaign);

    @Select("SELECT status FROM creatives_statuses WHERE id IN (SELECT statusId FROM creatives WHERE creatives.id = #{id})")
    CreativeStatus getStatus(Creative creative);

    @Select("SELECT type FROM creatives_types WHERE id IN (SELECT typeId FROM creatives WHERE creatives.id = #{id})")
    CreativeType getType(Creative creative);

    @Select("SELECT * FROM creatives_dimensions WHERE id IN (SELECT dimensionsId FROM creatives WHERE creatives.id = #{id})")
    CreativeDimension getDimensions(Creative creative);
}
