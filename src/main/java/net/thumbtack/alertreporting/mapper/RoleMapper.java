package net.thumbtack.alertreporting.mapper;

import net.thumbtack.alertreporting.entity.user.Role;
import net.thumbtack.alertreporting.entity.user.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RoleMapper {
    @Select("SELECT * FROM roles WHERE id IN (SELECT roleId FROM users_roles WHERE userId = #{id})")
    List<Role> getByUser(User user);

    @Select("SELECT * FROM roles")
    List<Role> getAll();
}
