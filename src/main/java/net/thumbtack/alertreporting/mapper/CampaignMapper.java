package net.thumbtack.alertreporting.mapper;

import net.thumbtack.alertreporting.entity.advertiser.Advertiser;
import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.campaign.CampaignStatus;
import net.thumbtack.alertreporting.entity.creative.Creative;
import net.thumbtack.alertreporting.entity.report.Report;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

@Mapper
public interface CampaignMapper {
    @Select("SELECT * FROM campaigns")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "advertiser", column = "id", javaType = Advertiser.class,
                    one = @One(select = "net.thumbtack.alertreporting.mapper.AdvertiserMapper.getByCampaign",
                            fetchType = FetchType.EAGER)),
            @Result(property = "status", column = "id", javaType = CampaignStatus.class,
                    one = @One(select = "net.thumbtack.alertreporting.mapper.CampaignMapper.getStatus",
                            fetchType = FetchType.EAGER)),
            @Result(property = "creatives", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.alertreporting.mapper.CreativeMapper.getByCampaign",
                            fetchType = FetchType.EAGER)),
            @Result(property = "categories", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.alertreporting.mapper.CategoryMapper.getByCampaign",
                            fetchType = FetchType.EAGER))})
    List<Campaign> getAll();

    @Select("SELECT * FROM campaigns WHERE id = #{id}")
    @ResultMap("getAll-void")
    Campaign getById(Integer id);

    @Select("SELECT * FROM campaigns WHERE advertiserId = #{id}")
    List<Campaign> getByAdvertiser(Advertiser advertiser);

    @Select("SELECT * FROM campaigns WHERE id = (SELECT campaignId FROM creatives WHERE id = #{id})")
    Campaign getByCreative(Creative creative);

    @Select("SELECT * FROM campaigns WHERE id IN (SELECT campaignId FROM reports WHERE id = #{id})")
    @ResultMap("getAll-void")
    List<Campaign> getByReport(Report report);

    @Select("SELECT status FROM campaigns_statuses WHERE id IN (SELECT statusId FROM campaigns WHERE campaigns.id = #{id})")
    CampaignStatus getStatus(Campaign campaign);

    @Select("SELECT * FROM campaigns WHERE archived = #{archived}")
    @ResultMap("getAll-void")
    List<Campaign> getByArchivedStatus(Boolean archived);

    @Select("SELECT * FROM campaigns WHERE id IN (SELECT campaignId FROM users_campaigns WHERE userId = #{id})")
    @ResultMap("getAll-void")
    List<Campaign> getByUserId(Integer id);

    @Select("SELECT * FROM campaigns WHERE id NOT IN (SELECT campaignId FROM users_campaigns)")
    @ResultMap("getAll-void")
    List<Campaign> getNotAssigned();
}
