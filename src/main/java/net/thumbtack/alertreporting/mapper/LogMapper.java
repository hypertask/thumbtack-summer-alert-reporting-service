package net.thumbtack.alertreporting.mapper;

import net.thumbtack.alertreporting.entity.logmessage.LogMessage;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface LogMapper {
    @Insert("INSERT INTO logs VALUES(DEFAULT, #{username}, #{action}, #{created})")
    @Options(useGeneratedKeys = true)
    Integer insert(LogMessage logMessage);

    @Select("SELECT * FROM logs ORDER BY created DESC")
    List<LogMessage> getAll();
}
