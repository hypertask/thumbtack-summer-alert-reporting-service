package net.thumbtack.alertreporting.mapper;

import net.thumbtack.alertreporting.entity.campaign.Campaign;
import net.thumbtack.alertreporting.entity.user.Role;
import net.thumbtack.alertreporting.entity.user.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

@Mapper
public interface UserMapper {

    @Insert("INSERT INTO users VALUES (DEFAULT, #{username}, #{password}, #{email}, #{firstName}, #{lastName}, #{patronymic})")
    @Options(useGeneratedKeys = true)
    Integer insert(User user);

    @Insert("INSERT INTO users_roles VALUES (DEFAULT, #{user.id}, #{role.id})")
    Integer addRole(@Param("user") User user, @Param("role") Role role);

    @Delete("DELETE FROM users_roles WHERE userId = #{id}")
    Integer deleteRoles(User user);

    @Insert("INSERT INTO users_campaigns VALUES(DEFAULT, #{userId}, #{campaignId})")
    Integer addCampaign(@Param("userId") Integer userId, @Param("campaignId") Integer campaignId);

    @Delete("DELETE FROM users_campaigns WHERE userId = #{userId} AND campaignId = #{campaignId}")
    Integer removeCampaign(@Param("userId") Integer userId, @Param("campaignId") Integer campaignId);

    @Update("UPDATE users SET username = #{username}, password = #{password}, email = #{email}, firstName = #{firstName}, " +
            "lastName = #{lastName}, patronymic = #{patronymic} WHERE id = #{id}")
    Integer update(User user);

    @Delete("DELETE FROM users WHERE id = #{id}")
    Integer delete(Integer id);

    @Select("SELECT * FROM users WHERE username = #{username}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "roles", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.alertreporting.mapper.RoleMapper.getByUser",
                            fetchType = FetchType.EAGER))})
    User getByUsername(String username);

    @Select("SELECT * FROM users WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "roles", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.alertreporting.mapper.RoleMapper.getByUser",
                            fetchType = FetchType.EAGER))})
    User getById(Integer id);

    @Select("SELECT * FROM users")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "roles", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.alertreporting.mapper.RoleMapper.getByUser",
                            fetchType = FetchType.EAGER))})
    List<User> getAll();

    @Select("SELECT * FROM users WHERE id IN (SELECT userId FROM users_campaigns WHERE campaignId = #{id})")
    @ResultMap("getAll-void")
    List<User> getByCampaign(Campaign campaign);
}
