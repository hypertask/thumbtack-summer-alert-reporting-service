# Alert-Reporting Service

## Release notes:

### Version 2.0.0 - 31/07/2017

    - new ui that supports user management features
    - new validation rules

### Version 1.3.0 - 21/07/2017

    - add endpoints for user management
    - now campaigns required fields rule checks campaign price
    - add rule to check campaign price is not lower than creatives price
    - add rule to check campaigns active creatives have same price

### Version 1.2.0 - 19/07/2017

    - add rule to check archived campaign status

### Version 1.1.2 - 18/07/2017

    - fix show reports for campaigns without managers to all users

### Version 1.1.1 - 18/07/2017

    - fix validation works from curl
    - fix ui shows users reports after validation

### Version 1.1.0 - 14/07/2017

    - add authentication and authorization with role-based access
    - add basic ui
    - add possibility to filter reports by user
    - now show entity ID in issues
    - now advertiser validations generate reports without campaignId and campaignName
    - now creatives validations generate reports without campaignId and campaignName if have no campaign

### Version 1.0.1 - 11/07/2017

    - now reports grouped by campaing
    - now no reports generated on validation failure

### Version 1.0.0 - 07/07/2017

    - initial release
    - add validation with rules choosing feature
    - add reports viewing feature

